# Aquavias

Lancement du jeu :

→ aller dans le dossier interface_serialization

→ pour la partie interface graphique : make run

→ pour la partie terminal : javac Launcher.java + java Launcher

→ N'oubliez pas de faire un make clean après avoir fini le jeu !


Organisation des fichiers :

-Logique du jeu : Jeu.java, Niveau.java, Piece.java, PieceI.java, PieceL.java, PieceP.java, PieceT.java, Serialization.java

-Vue terminal : Terminal.java, Launcher.java (qui contient le main)

-Vue interface graphique : JPanelFond.java, JScrollPaneDemo.java, Master.java, Music.java, Panneau.java, SimpleCritere.java, 

 SimpleJButtonSer.java, Test.java (qui contient le main) ,Timer.java, VueCreation.java, Vue.java, VueJoueur.java, VueNiveau.java, VueSuppression.java 