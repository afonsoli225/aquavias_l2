import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import javax.imageio.ImageIO;
import javax.swing.*;
public class JPanelFond extends JPanel {
  private static final long serialVersionUID = 1L;
  private int i;

  public JPanelFond(int a){
    this.i=a;
    
  }
  //Image du menu avec le gros AZUL
  public void paintComponent(Graphics g) {
    
      try {
        Image img = ImageIO.read(new File("Images/background"+Integer.toString(this.i)+".png"));
        g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
      } catch (IOException e) {
        e.printStackTrace();
      }
   
      
    
    }
  
}
