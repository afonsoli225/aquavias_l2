import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;


public class JScrollPaneDemo extends Thread {
  private Vue accueil;
  public JScrollPaneDemo(Vue v){
    this.accueil=v;
  }
  public void run(){

      JFrame f = new JFrame("Informations sur notre jeu");
      JPanel panel = new JPanel();

      panel.setLayout(new GridLayout(0, 1));
      JTextArea a=new JTextArea("Aquavias est un jeu de logique dans lequel une source d'eau doit être reliée à une arrivée pour pouvoir permettre l'acheminement de celle-ci.Pour se faire, le niveau commence avec des pièces mal placées qu'on pourra faire tourner en cliquant dessus.Lorsqu'une pièce est tournée dans la bonne position et permet à l'eau de passer, elle est affichée en bleue.Le but est qu'un chemin de pièces bleues relient la source d'eau à l'arrivée.\nSelon la difficulté du niveau on pourra avoir plusieurs obstacles comme :\n-un nombre de coups maximum pré défini.\n-pas de fuites d'eau autorisées.\n-minuteur pré défini à respecter.\nAu fur et à mesure que les chemins d'eau seront résolus, cela permettra d'irriguer la ville et de permettre son développement. Vous gagnerez le jeu quand la ville sera sauvée de la sécheresse.");
      a.setPreferredSize(new Dimension(500, 250));
      a.setLineWrap(true);
      a.setWrapStyleWord(true);
      a.setOpaque(false);
      a.setEditable(false);
      panel.add(a);
      JLabel hyperlink=new JLabel();
      hyperlink.setHorizontalAlignment(JLabel.CENTER);
      hyperlink.setForeground(Color.BLUE.darker());
      hyperlink.setCursor(new Cursor(Cursor.HAND_CURSOR));

      JLabel hyperlink2=new JLabel();
      hyperlink2.setHorizontalAlignment(JLabel.CENTER);
      hyperlink2.setForeground(Color.BLUE.darker());
      hyperlink2.setBackground(Color.BLUE);
      hyperlink2.setCursor(new Cursor(Cursor.HAND_CURSOR));
      hyperlink.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          try {
            Desktop.getDesktop().browse(new URI("https://youtu.be/dnJCfwHMGI0"));
          } catch (IOException | URISyntaxException e1) {
            e1.printStackTrace();
          }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                hyperlink.setText("Notre crew");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hyperlink.setText("<html><a href=''>" + "Notre crew" + "</a></html>");
            }

      });
      hyperlink2.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI("https://www.youtube.com/watch?v=bw2U3PiMeDM"));
                } catch (IOException | URISyntaxException e1) {
                    e1.printStackTrace();
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                hyperlink2.setText("Vidéo démo");
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                hyperlink2.setText("<html><a href=''>" + "Vidéo démo" + "</a></html>");
            }
      });
      panel.add(hyperlink);
      panel.add(hyperlink2);


      JButton retour = new JButton("Retour à l'accueil");
      retour.setPreferredSize(new Dimension(100, 100));
      panel.add(retour);
      retour.addActionListener( event ->
          {
          f.dispose();
          accueil.getFrame().setEnabled(true);
          accueil.getTextField().addFocusListener(new FocusListener() {
            @Override
            public void focusLost(final FocusEvent pE) {
            }
            @Override
            public void focusGained(final FocusEvent pE) {
            accueil.getTextField().selectAll();
            }
            });
          accueil.getFrame().validate();
          accueil.getTextField().requestFocus();
          }
      );
      JPanel container = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
      container.add(panel);
      accueil.getFrame().setEnabled(false);
      f.setAlwaysOnTop(true);
      JScrollPane scrollPane = new JScrollPane(container);
      f.getContentPane().add(scrollPane);
      f.setSize(550,200);
      f.setLocationRelativeTo(null);
      f.setVisible(true);
      f.setDefaultCloseOperation(0);
  }

}
