import java.util.*;
import java.io.*;
import java.util.HashMap;

public class Jeu {
  private ArrayList<Niveau> niveaux = new ArrayList<Niveau>();
  private Panneau pan;
  private HashMap<Niveau, String> level= new HashMap<Niveau, String>();;

  public Jeu () {
    //this.niveaux = new Niveau[20];
    read();
  }
  public void read() {
    int indice=0;
    while(indice!=100){
      File tmpDir = new File(Integer.toString(indice)+".ser");
      boolean exists = tmpDir.exists();
      if(exists==true){
        Serialization ser1 = new Serialization();
        ser1.deserialize(Integer.toString(indice)+".ser");
        this.pan = ser1.getPanneau();
        this.level.put(this.pan.getNiveau(), Integer.toString(indice));
        niveaux.add(this.pan.getNiveau());
      }
      indice++;
    }
    //niveaux.add(null);
    Collections.sort(this.niveaux);
    //TreeMap<Niveau, String> sorted = new TreeMap<>();
    //sorted.putAll(this.level);
  }
  public HashMap<Niveau, String> getLevel(){
    return this.level;
  }
  public ArrayList<Niveau> getNiveaux(){
    return niveaux;
  }

}
