import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Hashtable;
import javax.swing.border.*; 

public class Master extends JFrame {
	private JButton supprimer = new JButton("SUPPRIMER");
	private JButton creer= new JButton("CREER");
	private JButton retour= new JButton("REVENIR AU MENU");
	private JPanel container = new JPanel();
	

	public Master(){
		start();
	}
	public void start(){
		//Les boutons avec une certaine taille
		supprimer.setContentAreaFilled(false);
		supprimer.setOpaque(true);
		creer.setContentAreaFilled(false);
		creer.setOpaque(true);
		retour.setContentAreaFilled(false);
		retour.setOpaque(true);
		this.creer.setPreferredSize(new Dimension(600, 200));
		this.supprimer.setPreferredSize(new Dimension(600, 200));
		this.retour.setPreferredSize(new Dimension(600, 200));
		Border blackline= BorderFactory.createLineBorder(Color.black);
		this.supprimer.setBorder(blackline);
		this.creer.setBorder(blackline);
		this.retour.setBorder(blackline);
    
		
		
		//Creation dun JPanel avec un layout grille
		JPanel pan=new JPanel();
		this.container.setBackground(Color.BLACK);
		GridLayout experimentLayout = new GridLayout(3,1);
		pan.setLayout(experimentLayout);
		this.container.add(pan, BorderLayout.CENTER);
		//Rajout des boutons
		pan.add(this.supprimer);
		pan.add(this.creer);
		pan.add(this.retour);

		//fonctionnalité des boutons
		supprimer.addActionListener( event ->
      		{
      			VueSuppression vue=new VueSuppression();
      			this.dispose();
        	}
   		);
   		creer.addActionListener( event ->
      		{
      			SimpleCritere txt = new SimpleCritere();
      			this.dispose();
        	}
   		);
   		retour.addActionListener( event ->
      		{
      			Vue vue=new Vue();
      			this.dispose();
        	}
   		);
   		
   		supprimer.addMouseListener(new java.awt.event.MouseAdapter() {
   			Font n=supprimer.getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		supprimer.setBorderPainted(false);
        	supprimer.setBackground(Color.ORANGE);
        	supprimer.setFont(new Font(n.getName(),n.getStyle(),20));

    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
        	supprimer.setBackground(UIManager.getColor("control"));
        	supprimer.setBorderPainted(true);
        	supprimer.setFont(n);
    		}
    		@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	supprimer.setBorderPainted(false);
        	supprimer.setBackground(Color.GRAY);
        	}
		});
		creer.addMouseListener(new java.awt.event.MouseAdapter() {
			Font n=creer.getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		creer.setBorderPainted(false);
        	creer.setBackground(Color.ORANGE);
        	creer.setFont(new Font(n.getName(),n.getStyle(),20));
    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
    		creer.setBackground(UIManager.getColor("control"));
    		creer.setBorderPainted(true);
    		creer.setFont(n);
        	}
        	@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	creer.setBorderPainted(false);
        	creer.setBackground(Color.GRAY);
        	}
		});
		retour.addMouseListener(new java.awt.event.MouseAdapter() {
			Font n=retour.getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		retour.setBorderPainted(false);
        	retour.setBackground(Color.ORANGE);
        	retour.setFont(new Font(n.getName(),n.getStyle(),20));

    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
    		retour.setBackground(UIManager.getColor("control"));
    		retour.setBorderPainted(true);
    		retour.setFont(n);
        	}
        	@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	retour.setBorderPainted(false);
        	retour.setBackground(Color.GRAY);
        	}
		});

		this.setContentPane(container);
    	this.pack();
    	this.setLocationRelativeTo(null);
    	this.setVisible(true);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}