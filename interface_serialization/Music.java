import java.io.File;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
public class Music{

	private Clip clip=null;

	public Music(){

	}
	void playMusic(String location){
		try{
			File path=new File(location);
			if(path.exists()){
				AudioInputStream a=AudioSystem.getAudioInputStream(path);
				clip=AudioSystem.getClip();
				clip.open(a);
				clip.start();
				clip.loop(Clip.LOOP_CONTINUOUSLY);


			}else{
				System.out.println("Music not found");
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	void stopMusic(String location){
		try{
			File path=new File(location);
			if(path.exists()){
				
				clip.close();
				clip.stop();
				


			}else{
				System.out.println("Music not found");
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
	void playMusicWithoutLoop(String location){
		try{
			File path=new File(location);
			if(path.exists()){
				AudioInputStream a=AudioSystem.getAudioInputStream(path);
				clip=AudioSystem.getClip();
				clip.open(a);
				clip.start();
				


			}else{
				System.out.println("Music not found");
			}

		}catch(Exception e){
			e.printStackTrace();
		}
	}
}