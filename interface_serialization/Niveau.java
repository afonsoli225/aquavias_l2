import java.io.Serializable;
import java.util.List;
import java.util.ArrayList;
public class Niveau implements Serializable , Comparable<Niveau>{

    private int numero;
    private Piece[][] pieces;
    private Piece depart;
    private Piece arrivee;
    private Piece modif; //pour retrouver la piece qui vient d'être sélectionnnée
    private int fuites; //entier qui détermine le nombre de fuites dans le niveau
    private List<Niveau> parcours; //liste de tous les parcours disponibles pour résoudre le niveau
    private int min_coups; //le nombre minimum de coups possible
    private int max_coups; //le nombre maximum de coups possible (en supposant que l'on ne fait pas de coup inutile)
    //new
    private boolean[] option={false,false};
    private static int nbre_coup;

    public Niveau(int n, Piece[][] p, Piece d, Piece a) {
        this.numero = n;
        this.pieces = p;
        this.depart = d;
        this.arrivee = a;
        this.modif = new Piece(0);
        this.fuites = 0;
        this.parcours = new ArrayList<Niveau>();
        this.min_coups = 0;
        this.max_coups = 0;
        for(int i=0;i<p.length;i++) {
          for(int j=0;j<p[0].length;j++) {
            if( pieces[i][j]!= null) {
              pieces[i][j].setX(i);
              pieces[i][j].setY(j);
            }
            if( pieces[i][j] == this.depart) {
              this.depart.setX(i);
              this.depart.setY(j);
            }
            else if( pieces[i][j] == this.arrivee) {
              this.arrivee.setX(i);
              this.arrivee.setY(j);
            }
          }
        }
    }
    public Niveau(int n,int x,int y){
      this.numero=n;
      this.pieces = new Piece[x][y];
      for (int i=0;i<x;i++){
        for(int j=0;j<y;j++){
          pieces[i][j]=null;
        }
      }

      this.depart = null;
      this.arrivee = null;
      this.modif = new Piece(0);
      this.fuites = 0;
      this.parcours = new ArrayList<Niveau>();
      this.min_coups = 0;
      this.max_coups = 0;
    }

    public void setPiece(Piece p, int x,int y){
      this.pieces[x][y]=p;
    }
    public void setPieceD(Piece p,int x,int y){
      this.depart=p;
      this.pieces[x][y]=this.depart;

    }
    public void setPieceA(Piece p,int x,int y){
      this.arrivee=p;
      this.pieces[x][y]=this.arrivee;
    }

    public int getNumero() {
        return this.numero;
    }

    public Piece getArrivee() {
      return this.arrivee;
    }
    public Piece getDepart(){
      return this.depart;
    }

     public Piece getModif() {
       return this.modif;
     }

     public void setModif(Piece p) {
       this.modif = p;
     }

     public int getFuites() {
       return this.fuites;
     }

    public Piece[][] getPieces(){
      return this.pieces;
    }

    public List<Niveau> getParcours() {
      return this.parcours;
    }
    //new
    public void cleanOption(){
      this.option[0]=false;
      this.option[1]=false;
    }
    //new
    public void setOption(int i,boolean b){
      if(i==0||i==1)  this.option[i]=b;
      else{
        this.option[0]=b;
        this.option[1]=b;
      }
    }
    //new 
    public boolean[] getOption(){
      return this.option;
    }
    //new
    public int getNbreCoups(){
      return this.nbre_coup;
    }
    public void dimimuerNbreCoups(){
      this.nbre_coup--;
    }
    public void setNbreCoups(int a){
      this.nbre_coup=a;
    }

    public int getMin() {
      return this.min_coups;
    }

    public void setMin(int m) {
      this.min_coups = m;
    }

    public int getMax() {
      return this.max_coups;
    }

    public void setMax(int m) {
      this.max_coups = m;
    }
    public int nbrDepart(){
      int tmp=0;
      for (int i=0;i<this.pieces.length;i++){
        for(int j=0;j<this.pieces[0].length;j++){
          if(pieces[i][j] instanceof PieceP&&pieces[i][j].getRotat()==6){
            tmp++;
          }
        }
      }
      return tmp;
    }
    public int nbrArrivee(){
      int tmp=0;
      int h=this.pieces.length;
      int l=this.pieces[0].length;
      for (int i=0;i<h;i++){
        for(int j=0;j<l;j++){
          if(pieces[i][j] instanceof Piece&&pieces[i][j].getRotat()==1&&i==0 &&(j>0||j<l)){
            tmp++;
          }else if(pieces[i][j] instanceof Piece&&pieces[i][j].getRotat()==2&&i==h-1&&(j>0||j<l)){
            tmp++;
          }else if(pieces[i][j] instanceof Piece&&pieces[i][j].getRotat()==3&&j==0){
            tmp++;
          }else if(pieces[i][j] instanceof Piece&&pieces[i][j].getRotat()!=6&&j==l-1&&(i>0||i<h)){
            tmp++;
          }
        }
      }
      return tmp;
    }
    //emboitement des pieces
    public boolean emboite(Piece p1, Piece p2) {
      if(p1 == null || p2 == null) {
        return false;
      }
      int x1 = p1.getX();
      int y1 = p1.getY();
      int x2 = p2.getX();
      int y2 = p2.getY();

      if( x1 == x2 ){
        if( y1 == y2-1 ) return( p1.getRotation()[1] && p2.getRotation()[3] );
        else if( y1 == y2+1 ) return( p1.getRotation()[3] && p2.getRotation()[1] );
        else return false;
      }
      else if( y1 == y2 ) {
        if( x1 == x2-1 ) return( p1.getRotation()[2] && p2.getRotation()[0] );
        else if( x1 == x2+1 ) return( p1.getRotation()[0] && p2.getRotation()[2] );
        else return false;
      }
      else return false;
    }

    //test d'emboitement des pieces à partir de la piece de depart du niveau
    public void emboitement_depart() {
      int x = this.depart.getX();
      int y = this.depart.getY();

      //on remet les fuites dans le niveau à jour
      this.fuites = 1; //si jamais la piece de depart ne s'emboite pas avec la suivante il y a bien une fuite

      //si la piece que l'on vient de tourner n'est pas reliée à d'autres pieces on retire l'eau (sinon elle ne sera jamais testée par la suite)
      if( estSeule(this.modif) ) {
        pieces[this.modif.getX()][this.modif.getY()].setEau(false);
      }

      //CAS OU LA PIECE DE DEPART EST AU NORD
      if( x == 0 ) {
        emboitement_depart_aux(x+1,y);
      }

      //CAS OU LA PIECE DE DEPART EST AU SUD
      if( x == pieces.length-1 ) {
        emboitement_depart_aux(x-1,y);
      }

      //CAS OU LA PIECE DE DEPART EST A L'OUEST
      if( y == 0 ) {
        emboitement_depart_aux(x,y+1);
      }

      //CAS OU LA PIECE DE DEPART EST A L'EST
      if( y == pieces[x].length-1 ) {
        emboitement_depart_aux(x,y-1);
      }
    }

    public void emboitement_depart_aux(int s, int t) {
      int x = this.depart.getX();
      int y = this.depart.getY();

      if(emboite(pieces[x][y], pieces[s][t])) {
        this.fuites = 0;
        pieces[s][t].setEau(true);
        emboitement_rec(pieces[s][t]);
      }
      else if( this.modif == pieces[s][t] ) {
        Piece r = pieces[s][t].retourner();
        retireEau(r);
      }
      else retireEau(pieces[s][t]);
    }

    //teste les emboitements autour d'une piece p de facon recursive
    public void emboitement_rec(Piece p) {
      int x = p.getX();
      int y = p.getY();
      int mx = modif.getX();
      int my = modif.getY();
      p.setTest(true); //on met le booléen test à true, le test de cette piece est bien effectué

      if(p.getRotation()[0] && !emboite(pieces[x][y], pieces[x-1][y]) ) this.fuites++; //nord
      if(p.getRotation()[2] && !emboite(pieces[x][y], pieces[x+1][y]) ) this.fuites++; //sud
      if(p.getRotation()[3] && !emboite(pieces[x][y], pieces[x][y-1]) ) this.fuites++; //ouest
      if(p.getRotation()[1] && !emboite(pieces[x][y], pieces[x][y+1]) ) this.fuites++; //est

      //CAS NORD
      if(x > 1 && this.pieces[x-1][y]!=null) { // on verifie que l'on n'est pas sur le bord superieur
        emboitement_rec_aux(p, x-1, y);
      }

      //CAS SUD
      if(x < pieces.length-2 && this.pieces[x+1][y]!=null) { // on verifie que l'on n'est pas sur le bord inferieur
        emboitement_rec_aux(p, x+1, y);
      }

      //CAS OUEST
      if(y > 1 && this.pieces[x][y-1]!=null) { // on verifie que l'on n'est pas sur le bord gauche
        emboitement_rec_aux(p, x, y-1);
      }

      //CAS EST
      if(y < pieces[x].length-2 && this.pieces[x][y+1]!=null) { // on verifie que l'on n'est pas sur le bord droit
        emboitement_rec_aux(p, x, y+1);
      }
  }

    public void emboitement_rec_aux(Piece p, int u, int t) {
      int x = p.getX();
      int y = p.getY();
      int mx = modif.getX();
      int my = modif.getY();
      Piece s = pieces[u][t];

      if( emboite(pieces[x][y],pieces[u][t]) ) {
        pieces[u][t].setEau(true); //l'eau passe dans la piece suivante

        //on verifie que la piece suivante n'a pas déjà été testée et on appelle la fonction à nouveau pour la piece suivante
        if(!pieces[u][t].getTest()) {
          emboitement_rec(pieces[u][t]);
        }
      }

      else {

        //dans le cas où les pieces ne s'emboitent plus mais que l'eau passait avant lorsqu'on est la piece qui vient d'être tournée
        Piece r = p.retourner();
        if( pieces[u][t].getEau() && !pieces[u][t].getTest() && (x == mx) && (y == my) && emboite(r,pieces[u][t]) ) {
          //fonction qui retire l'eau sur son chemin
          retireEau(pieces[u][t]);
        }

        if( s.getX() == mx && s.getY() == my ) {
          s = s.retourner();
          if( emboite(s, p) && !pieces[u][t].getTest() ) {
            //fonction qui retire l'eau sur son chemin
            retireEau(s);
          }
        }
      }
    }

    //teste si la piece que l'on vient de deplacer ne s'emboite avec aucune autre piece
    public boolean estSeule(Piece p) {
      int mx = p.getX();
      int my = p.getY();

      //dans le cas où la fonction est appelée avant qu'une seule piece n'ait été choisie
      if( mx == 0 && my == 0) {
        return false;
      }

      if( (mx > 1 && emboite(p, pieces[mx-1][my]) ) || (mx < pieces.length-2 && emboite(p, pieces[mx+1][my])) || (my > 1 && emboite(p, pieces[mx][my-1])) || (my < pieces[mx].length-2 && emboite(p, pieces[mx][my+1])) ) {
        return false;
      }
      return true;
    }

    public void retireEau(Piece p) {
      if(p !=null){
        int x = p.getX();
        int y = p.getY();
        pieces[x][y].setEau(false);
        pieces[x][y].setTest2(true);

        //CAS NORD
        if (x > 1 && emboite(p,pieces[x-1][y]) && !pieces[x-1][y].getTest2() && !pieces[x-1][y].getTest()) retireEau(pieces[x-1][y]);

        //CAS SUD
        if (x < pieces.length-1 && emboite(p,pieces[x+1][y]) && !pieces[x+1][y].getTest2() && !pieces[x+1][y].getTest()) retireEau(pieces[x+1][y]);

        //CAS OUEST
        if (y > 1 && emboite(p,pieces[x][y-1]) && !pieces[x][y-1].getTest2() && !pieces[x][y-1].getTest()) retireEau(pieces[x][y-1]);

        //CAS EST
        if (y < pieces[x].length-1 && emboite(p,pieces[x][y+1]) && !pieces[x][y+1].getTest2() && !pieces[x][y+1].getTest()) retireEau(pieces[x][y+1]);
      }
    }


    //fonction de test de l'eau pour la piece d'arrivée
    public void eauArrivee() {
      int x = this.arrivee.getX();
      int y = this.arrivee.getY();

      //CAS OU LA PIECE D'ARRIVEE EST AU NORD
      if( x == 0 && emboite(this.arrivee, pieces[x+1][y]) && this.pieces[x+1][y].getEau() ) this.arrivee.setEau(true);

      //CAS OU LA PIECE D'ARRIVEE EST AU SUD
      else if( x == this.pieces.length-1 && emboite(this.arrivee, this.pieces[x-1][y]) && this.pieces[x-1][y].getEau() ) this.arrivee.setEau(true);

      //CAS OU LA PIECE D'ARRIVEE EST A L'OUEST
      else if( y == 0 && emboite(this.arrivee, this.pieces[x][y+1]) && this.pieces[x][y+1].getEau() ) this.arrivee.setEau(true);

      //CAS OU LA PIECE D'ARRIVEE EST A L'EST
      else if( y == this.pieces[x].length-1 && emboite(this.arrivee, this.pieces[x][y-1]) && this.pieces[x][y-1].getEau() ) this.arrivee.setEau(true);

      else {
        this.arrivee.setEau(false);
      }

    }

    //remet tous les booléens test de toutes les pièces du niveau à false
    public void maj_bool_test() {
      for(int i = 1; i < pieces.length-1; i++) {
        for(int j = 1; j < pieces[i].length-1; j++) {
          if(pieces[i][j]!=null){
            pieces[i][j].setTest(false);
            pieces[i][j].setTest2(false);
          }

        }
      }
    }

    //fonction de detection de la victoire
    public boolean victoire() {
      if(this.fuites == 0 && this.arrivee.getEau()) return true;
      return false;
    }

    //methode pour tester si la piece p est proche de l'arrivée ou du départ (piece test)
    public boolean proche(Piece p, Piece test) {
      int x = p.getX();
      int y = p.getY();
      int ax = test.getX();
      int ay = test.getY();

      //CAS OU LA PIECE D'ARRIVEE EST AU NORD
      if( ax == 0 && x == ax+1 && ay == y ) return true;

      //CAS OU LA PIECE D'ARRIVEE EST AU SUD
      else if( ax == this.pieces.length-1 && x == ax-1 && ay == y ) return true;

      //CAS OU LA PIECE D'ARRIVEE EST A L'OUEST
      else if( ay == 0 && y == ay+1 && ax == x ) return true;

      //CAS OU LA PIECE D'ARRIVEE EST A L'EST
      else if( ay == this.pieces[ax].length-1 && y == ay-1 && ax == x ) return true;

      return false;
    }

    //vérifie si une fuite est sûre d'arriver (dans le cas où la piece p est au bord et l'eau s'écoule de ce côté là)
    public boolean fuiteAssuree(Piece p) {
      int x = p.getX();
      int y = p.getY();

      //CAS NORD
      if( x == 1 && p.getRotation()[0] ) return true;

      //CAS SUD
      if( x == this.pieces.length-2 && p.getRotation()[2] ) return true;

      //CAS OUEST
      if( y == 1 && p.getRotation()[3] ) return true;

      //CAS EST
      if( y == this.pieces[x].length-2 && p.getRotation()[1] ) return true;

      return false;
    }

    //vérifie si le niveau actuel est identique au niveau niv
    public boolean niveauIdentique(Niveau niv) {
      if( this.pieces.length != niv.pieces.length || this.pieces[0].length != niv.pieces[0].length ) return false;

      for( int i = 0; i < this.pieces.length; i++ ) {
        for( int j = 0; j < this.pieces[i].length; j++ ) {
          if( this.pieces[i][j] != null && niv.pieces[i][j] != null ) {
            //si ce n'est pas le même type de pièce ou si leur rotation n'est pas la même
            if(this.pieces[i][j].getClass() != niv.pieces[i][j].getClass() || (this.pieces[i][j].getClass() == niv.pieces[i][j].getClass() && this.pieces[i][j].getRotat() != niv.pieces[i][j].getRotat())) {
              return false;
            }
          }
        }
      }
      return true;
    }

    //vérifie si le niveau niv appartient à la liste des parcours du niveau actuel
    public boolean appartient(Niveau niv) {
      for( int i = 0; i < this.parcours.size(); i++ ) {
        if( niv.niveauIdentique(this.parcours.get(i))) return true;
      }
      return false;
    }

    //METHODES POUR EVALUER LA DIFFICULTE D'UN NIVEAU
    public void depart() {
      Niveau niv = copie();
      int x = this.depart.getX();
      int y = this.depart.getY();

      //CAS OU LA PIECE DE DEPART EST AU NORD
      if( x == 0 && niv.pieces[x+1][y] != null ) {
        backtracking(niv, niv.pieces[x+1][y], niv.depart);
      }

      //CAS OU LA PIECE DE DEPART EST AU SUD
      if( x == this.pieces.length-1 && niv.pieces[x-1][y] != null ) {
        backtracking(niv, niv.pieces[x-1][y], niv.depart);
      }

      //CAS OU LA PIECE DE DEPART EST A L'OUEST
      if( y == 0 && niv.pieces[x][y+1] != null ) {
        backtracking(niv, niv.pieces[x][y+1], niv.depart);
      }

      //CAS OU LA PIECE DE DEPART EST A L'EST
      if( y == this.pieces[x].length-1 && niv.pieces[x][y-1] != null ) {
        backtracking(niv, niv.pieces[x][y-1], niv.depart);
      }
    }

    public void backtracking(Niveau niv, Piece p, Piece appel) {
      int x = p.getX();
      int y = p.getY();
      int fin = 4; //permet de connaîre la fin de la boucle en fonction du type de pièce (pour éviter que les pieces I et P se répètent)
      if( p instanceof PieceP ) fin = 1;
      else if( p instanceof PieceI ) fin = 2;

      for( int i = 0; i <= fin; i++ ) {
        if( i != 0 ) p.tourner();
        //on fait tourner le jeu normalement
        niv.setModif(p);
        niv.jeu();

        //on a remis la piece dans sa rotation d'origine on peut alors sortir de la boucle for
        if( i == fin ) {
          fin = 0;
          if( p instanceof PieceI ) fin = 2;
          else if( p instanceof PieceP ) fin = 3;
          for( int j =0; j < fin; j++ ) { p.tourner(); }
          break;
        }

        if( niv.victoire() ) {
          Niveau cop = niv.copie();
          if( !this.appartient(cop) ) this.parcours.add(cop);
          niv.arrivee.setEau(false);
          continue;
        }

        //on teste toutes les possibilités de piece suivante
        if( niv.emboite(p, appel) && ( !niv.fuiteAssuree(p) || niv.proche(p, niv.depart) || niv.proche(p, niv.arrivee) ) ) {
          //CAS NORD
          if(x > 1 && niv.pieces[x-1][y] != null && !niv.pieces[x-1][y].getBacktracking() && p.getRotation()[0] ) {
            backtracking_aux(niv, niv.pieces[x-1][y], p);
          }
          //CAS SUD
          if(x < niv.pieces.length-2 && niv.pieces[x+1][y] != null && !niv.pieces[x+1][y].getBacktracking() && p.getRotation()[2] ) {
            backtracking_aux(niv, niv.pieces[x+1][y], p);
          }
          //CAS OUEST
          if(y > 1 && niv.pieces[x][y-1] != null && !niv.pieces[x][y-1].getBacktracking() && p.getRotation()[3] ) {
            backtracking_aux(niv, niv.pieces[x][y-1], p);
          }
          //CAS EST
          if(y < niv.pieces[x].length-2 && niv.pieces[x][y+1] != null && !niv.pieces[x][y+1].getBacktracking() && p.getRotation()[1] ) {
            backtracking_aux(niv, niv.pieces[x][y+1], p);
          }
        }
      }
    }

    public void backtracking_aux(Niveau niv, Piece target, Piece source) {
      source.setBacktracking(true);
      backtracking(niv, target, source);
      source.setBacktracking(false);
    }

    public Niveau copie() {
      Piece[][] p = new Piece[this.pieces.length][this.pieces[0].length];

      Piece d = new PieceP(6);
      d.setEau(true);
      p[this.depart.getX()][this.depart.getY()] = d;

      int r = this.arrivee.getRotat();
      Piece a = new Piece(r);
      a.getRotation()[2] = true;
      a.getRotation()[0] = true;
      a.getRotation()[1] = true;
      a.getRotation()[3] = true;
      p[this.arrivee.getX()][this.arrivee.getY()] = a;

      for( int i = 1; i < p.length-1; i++ ) {
        for( int j = 1; j < p[i].length-1; j++ ) {
          if( this.pieces[i][j] != null ) {
            Piece tmp = this.pieces[i][j];
            int rotat = tmp.getRotat();
            if( tmp instanceof PieceP ) p[i][j] = new PieceP(rotat);
            else if( tmp instanceof PieceL ) p[i][j] = new PieceL(rotat);
            else if( tmp instanceof PieceI ) p[i][j] = new PieceI(rotat);
            else if( tmp instanceof PieceT ) p[i][j] = new PieceT(rotat);
          }
        }
      }

      Niveau nouv = new Niveau(0, p, d, a);
      return nouv;
    }

    /**calcule le nombre de coups d'un niveau dans l'état où il est en le comparant avec un niveau racine
      * on suppose ici que racine est le niveau à partir duquel est créé le niveau actuel
    **/
    public int nbrCoups( Niveau racine ) {
      int x = this.depart.getX();
      int y = this.depart.getY();

      //CAS OU LA PIECE DE DEPART EST AU NORD
      if( x == 0 ) {
        return nbrCoups_aux(racine, this.pieces[x+1][y]);
      }

      //CAS OU LA PIECE DE DEPART EST AU SUD
      if( x == pieces.length-1 ) {
        return nbrCoups_aux(racine, this.pieces[x-1][y]);
      }

      //CAS OU LA PIECE DE DEPART EST A L'OUEST
      if( y == 0 ) {
        return nbrCoups_aux(racine, this.pieces[x][y+1]);
      }

      //CAS OU LA PIECE DE DEPART EST A L'EST
      if( y == pieces[x].length-1 ) {
        return nbrCoups_aux(racine, this.pieces[x][y-1]);
      }
      this.maj_bool_test();
      return 0;
    }

    public int nbrCoups_aux( Niveau racine, Piece p ) {



      int x = p.getX();
      int y = p.getY();
      int res = 0;
      p.setTest(true);

      if( !proche(p, this.arrivee) ) {
        //CAS NORD
        if( x > 1 && this.pieces[x-1][y] != null && !this.pieces[x-1][y].getTest() && emboite(this.pieces[x-1][y], p) ) {
          res += nbrCoups_aux(racine, this.pieces[x-1][y]);
        }
        //CAS SUD
        if( x < this.pieces.length-2 && this.pieces[x+1][y] != null && !this.pieces[x+1][y].getTest() && emboite(this.pieces[x+1][y], p) ) {
          res += nbrCoups_aux(racine, this.pieces[x+1][y]);
        }
        //CAS OUEST
        if( y > 1 && this.pieces[x][y-1] != null && !this.pieces[x][y-1].getTest() && emboite(this.pieces[x][y-1], p) ) {
          res += nbrCoups_aux(racine, this.pieces[x][y-1]);
        }
        //CAS EST
        if( y < this.pieces[x].length-2 && this.pieces[x][y+1] != null && !this.pieces[x][y+1].getTest() && emboite(this.pieces[x][y+1], p) ) {
          res += nbrCoups_aux(racine, this.pieces[x][y+1]);
        }
      }

      //on fait tourner la piece autant de fois que nécessaire pour retrouver la rotation initiale


      if( p.getRotat() != racine.getPieces()[x][y].getRotat() ) {
        Piece tmp = p.retourner();

        for( int i = 0; i < 3; i++ ) {
          if( i != 0 ) tmp = tmp.retourner();
          res++;
          if( tmp.getRotat() == racine.getPieces()[x][y].getRotat() ) break;
        }
      }
      return res;

  }

    public void jeu() {
      this.emboitement_depart();
      this.eauArrivee();
      this.maj_bool_test();
    }

    //met à jour le nombre de solutions du niveau ainsi que le minimum et maximum de coups à partir de toutes les solutions
    public void trouverSolutions() {
      this.depart();
      System.out.print(this.parcours.size());
      if( this.parcours.size() != 0 ) {

        Niveau cp = this.parcours.get(0).copie();
        int nbr = cp.nbrCoups(this);
        this.min_coups = nbr;
        this.max_coups = nbr;
        for(int k = 1; k < this.parcours.size(); k++ ) {
          cp = this.parcours.get(k).copie();
          nbr = cp.nbrCoups(this);
          if( nbr < this.min_coups ) this.min_coups = nbr;
          if( nbr > this.max_coups ) this.max_coups = nbr;
        }
      }
    }

    @Override
    public int compareTo(Niveau niv) {
      int comparesolutions2 = (niv.getParcours().size()*10)/niv.getMin();
      int comparesolutions1 = (this.parcours.size()*10)/this.min_coups;
      if( comparesolutions2-comparesolutions1 == 0 ) {
        int comparecoups1 = (this.min_coups*1000)/(this.pieces.length*this.pieces[0].length);
        int comparecoups2 = (niv.getMin()*1000)/(niv.getPieces().length*niv.getPieces()[0].length);
        return comparecoups1-comparecoups2;
      }
      else return comparesolutions2-comparesolutions1;
    }
}
