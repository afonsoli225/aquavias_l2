import java.math.*;
import java.util.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.io.Serializable;

public class Panneau extends JPanel implements Serializable{

	private Niveau niv;
	private ArrayList<JLabel> labOff=new ArrayList<JLabel>();
	private Piece selected=null;
	private int indice;
  private boolean joueable=true;


	//initilisation du panneau avec hauteur puis largeur;
	public Panneau(int x, int y) {
		super();
    this.setLayout(new GridLayout(x,y));
		Piece depart=new PieceP(6);
		depart.setEau(true);
		Piece arrivee= new Piece(1);
		this.niv=new Niveau(0,new Piece[x][y],depart ,arrivee);//numero 0 par défaut
		int a=this.niv.getPieces().length;
		int b=this.niv.getPieces()[0].length;
		JLabel[] lab=new JLabel[a*b];
		ImageIcon iconLogo = new ImageIcon("Images/vide.png");
		for (int i=0;i<a*b;i++){
			lab[i]=new JLabel();
			Border border = LineBorder.createGrayLineBorder();
			lab[i].setBorder(border);
			this.labOff.add(lab[i]);
			lab[i].setIcon(iconLogo);
			this.add(lab[i]);
		}
    putPiece();
  }

  //initialiser le panneau avec un niveau existant
	public Panneau(Niveau n){
    super();
    this.setOpaque(false);
    n.emboitement_depart();
    n.maj_bool_test();
    this.niv=n;
    this.niv.emboitement_depart();
    this.niv.maj_bool_test();
    int x=n.getPieces().length;
    int y=n.getPieces()[0].length;
    this.setPreferredSize(new Dimension(x*50, 150));
    this.setLayout(new GridLayout(x,y));
     //mettre les images
    initilisation(x,y);
    n.emboitement_depart();
    n.maj_bool_test();
    //tourner pour jouer
    jouer();
	}

  //on set la piece selected avec une piece p
	public void setSelected(Piece p){
		this.selected=p;
	}
  public void setFuite(){

  }
  public void setNbreDeCoup(){

  }
	//Mettre la piece selected dans une position precise
	public void putPiece(){
	  for( int i=0;i<this.labOff.size();i++){
	    this.indice=i;

	    this.labOff.get(i).addMouseListener(new MouseAdapter(){
	      final int tmp=Panneau.this.indice;

	      public void mouseClicked(MouseEvent e){
	              int a=tmp/Panneau.this.niv.getPieces()[0].length;
	              int b=tmp%Panneau.this.niv.getPieces()[0].length;
	              int h=Panneau.this.niv.getPieces().length;
	              int l=Panneau.this.niv.getPieces()[0].length;
	              int sizeLabelH= Panneau.this.labOff.get(tmp).getHeight();
	              int sizeLabelW= Panneau.this.labOff.get(tmp).getWidth();

	              if(!(a==0||b==0||a==h-1||b==l-1)){
	                if(Panneau.this.selected instanceof PieceP && Panneau.this.selected.getRotat()!=6) putPiece_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceP.png");
	                else if(Panneau.this.selected instanceof PieceI ) putPiece_aux2(sizeLabelW, sizeLabelH, tmp, "Images/PieceI");
	                else if(Panneau.this.selected instanceof PieceL ) putPiece_aux2(sizeLabelW, sizeLabelH, tmp, "Images/PieceL");
	                else if(Panneau.this.selected instanceof PieceT ) putPiece_aux2(sizeLabelW, sizeLabelH, tmp, "Images/PieceT");
	              }
								else {
	                //Quand la piece selectionnée est une pièce départ
	                if(Panneau.this.selected instanceof PieceP&&Panneau.this.selected.getRotat()==6 &&Panneau.this.niv.nbrDepart()==0){
	                  Panneau.this.selected=Panneau.this.niv.getDepart();
	                  if(a==0 &&(b>0||b<l)) putPiece_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart1.png");
	                  else if(a==h-1&&(b>0||b<l)) putPiece_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart2.png");
	                  else if(b==0&&(a>0&&a<h)) putPiece_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart0.png");
	                  else putPiece_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart3.png");
									}
									//Quand la piece selectionnée est une pièce arrivée
									else if(Panneau.this.selected  instanceof Piece&&Panneau.this.selected.getRotat()==1 &&Panneau.this.niv.nbrArrivee()==0){
	                  if(a==0 &&(b>0||b<l)) putPiece_arr(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee3.png", 1, 2);
	                  else if(a==h-1&&(b>0||b<l)) putPiece_arr(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee1.png", 2, 0);
	                  else if(b==0&&(a>0&&a<h)) putPiece_arr(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee2.png", 3, 1);
	                  else putPiece_arr(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee0.png", 4, 3);
									}
	              }
	              if(Panneau.this.selected==null){
	                ImageIcon iconLogo = new ImageIcon("Images/vide.png");
	                Panneau.this.labOff.get(tmp).setIcon(iconLogo);
	                Piece p=new Piece(30);
	                p.setX(a);
	                p.setY(b);
	                p.setEau(false);
	                Panneau.this.niv.setPiece(null,a,b);
	              }
	              if(Panneau.this.selected!=null){
	                Panneau.this.selected.setX(a);
	                Panneau.this.selected.setY(b);
	                Panneau.this.niv.setPiece(Panneau.this.selected,a,b);
	              }else {}
	              Panneau.this.selected=null;
	              Panneau.this.niv.emboitement_depart();
	              Panneau.this.niv.maj_bool_test();
	              }
	        });
	    }
	}

	public void putPiece_aux2(int sizeLabelW, int sizeLabelH, int tmp, String nom) {
	  String s=nom+Integer.toString(Panneau.this.selected.getRotat())+".png";
	  putPiece_aux(sizeLabelW, sizeLabelH, tmp, s);
	}

	public void putPiece_aux(int sizeLabelW, int sizeLabelH, int tmp, String nom) {
	  ImageIcon iconLogo=new ImageIcon(new javax.swing.ImageIcon(getClass().getResource(nom)).getImage().getScaledInstance(sizeLabelW, sizeLabelH, Image.SCALE_SMOOTH));
	  Panneau.this.labOff.get(tmp).setIcon(iconLogo);
	}

	public void putPiece_arr(int sizeLabelW, int sizeLabelH, int tmp, String nom, int set, int get) {
		Panneau.this.niv.getArrivee().setRotat(set);
		Panneau.this.niv.getArrivee().getRotation()[get] = true;
		putPiece_aux(sizeLabelW, sizeLabelH, tmp, nom);
		Panneau.this.selected=Panneau.this.niv.getArrivee();
	}
  //initiliser les images dans les JLabels
   public void initilisation(int x,int y){
    int tmp=0;
    int a=0;
    int b=0;
    int h=0;
    int l=0;
    int c=0;
    int d=0;
    int taille=x-y;
    int tailleP=(25*taille*taille)/11+(625*taille)/11+400;

    for( int i=0;i<x*y;i++){
      tmp=i;
      JLabel cont=new JLabel();
      a=tmp/this.niv.getPieces()[0].length;
      b=tmp%this.niv.getPieces()[0].length;
      if(y>x) c=2;
      else d=2;
      if(x>y){
        h=(700-(x-y)*48)/y;
        l=700/x;
      }
      if(x==y){
        h=700/y;
        l=700/x;
      }else if (x<y){
        h=(700+(y-x)*65)/y;
        l=700/x;
      }

			if(this.niv.getPieces()[a][b] instanceof PieceP &&this.niv.getPieces()[a][b].getRotat()!=6) init_aux(h, l, cont, "Images/PieceP.png");
			else if(this.niv.getPieces()[a][b] instanceof PieceI ) init_aux2(h, l, cont, this.niv.getPieces()[a][b], "Images/PieceI");
			else if(this.niv.getPieces()[a][b] instanceof PieceL ) init_aux2(h, l, cont, this.niv.getPieces()[a][b], "Images/PieceL");
			else if(this.niv.getPieces()[a][b] instanceof PieceT ) init_aux2(h, l, cont, this.niv.getPieces()[a][b], "Images/PieceT");
			//piece depart
			else if(this.niv.getPieces()[a][b] instanceof PieceP&&this.niv.getPieces()[a][b].getRotat()==6 ){
			  if(a==0) init_aux(h, l, cont, "Images/PieceDepart1.png");
			  else if(b==0) init_aux(h, l, cont, "Images/PieceDepart0.png");
			  else if(a==this.niv.getPieces().length-1) init_aux(h, l, cont, "Images/PieceDepart2.png");
			  else init_aux(h, l, cont, "Images/PieceDepart3.png");
			}
			//piece arrivee
			else if(this.niv.getPieces()[a][b]  instanceof Piece&&(this.niv.getPieces()[a][b].getRotat()==1||this.niv.getPieces()[a][b].getRotat()==2||this.niv.getPieces()[a][b].getRotat()==3||this.niv.getPieces()[a][b].getRotat()==4)){
			  if(this.niv.getPieces()[a][b].getEau()){
			    if(a==0) init_aux(h, l, cont, "Images/PieceArriveeFin3.png");
			    else if(b==0) init_aux(h, l, cont, "Images/PieceArriveeFin2.png");
			    else if(a==this.niv.getPieces().length-1) init_aux(h, l, cont, "Images/PieceArriveeFin1.png");
			    else init_aux(h, l, cont, "Images/PieceArriveeFin.png");
			  }
				else{
			    if(a==0) init_aux(h, l, cont, "Images/PieceArrivee3.png");
			    else if(b==0) init_aux(h, l, cont, "Images/PieceArrivee2.png");
			    else if(a==this.niv.getPieces().length-1) init_aux(h, l, cont, "Images/PieceArrivee1.png");
			    else init_aux(h, l, cont, "Images/PieceArrivee0.png");
			  }
			}
			//piece nulle
			else if(this.niv.getPieces()[a][b]==null ||(!this.niv.getPieces()[a][b].getRotation()[0]&&!this.niv.getPieces()[a][b].getRotation()[1]&&!this.niv.getPieces()[a][b].getRotation()[2]&&!this.niv.getPieces()[a][b].getRotation()[3])){
				cont.setOpaque(false);
			  this.labOff.add(cont);
			  this.add(cont);
			}
		}
	}

	public void init_aux(int h, int l, JLabel cont, String nom) {
		ImageIcon iconLogo = new ImageIcon(new javax.swing.ImageIcon(getClass().getResource(nom)).getImage().getScaledInstance(h, l, Image.SCALE_SMOOTH));
		cont.setIcon(iconLogo);
		this.labOff.add(cont);
		this.add(cont);
	}

	public void init_aux2(int h, int l, JLabel cont, Piece p, String nom) {
		String s = nom+Integer.toString(p.getRotat());
		if(!p.getEau()) s = s+".png";
		else s = s+"Eau.png";
		init_aux(h, l, cont, s);
	}

	public void suite(int x,int y){
    int tmp=0;
    int a=0;
    int b=0;
    int h=0;
    int l=0;
    int sizeLabelW=this.labOff.get(0).getWidth();
    int sizeLabelH=this.labOff.get(0).getHeight();
    int arr=0;
    int stocka=0;
    int stockb=0;
    this.niv.emboitement_depart();
    this.niv.maj_bool_test();
    for( int i=0;i<x*y;i++){
      tmp=i;
      JLabel cont=new JLabel();
      a=tmp/this.niv.getPieces()[0].length;
      b=tmp%this.niv.getPieces()[0].length;

      if(this.niv.getPieces()[a][b] instanceof PieceP &&this.niv.getPieces()[a][b].getRotat()!=6){
        if(!this.niv.getPieces()[a][b].getEau()) suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceP.png");
        else suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PiecePEau.png");
      }
			else if(this.niv.getPieces()[a][b] instanceof PieceI ){
        if(!this.niv.getPieces()[a][b].getEau()){
          String s="Images/PieceI"+Integer.toString(this.niv.getPieces()[a][b].getRotat()%2)+".png";
          suite_aux(sizeLabelW, sizeLabelH, tmp, s);
        }else{
          String s="Images/PieceI"+Integer.toString(this.niv.getPieces()[a][b].getRotat()%2)+"Eau.png";
          suite_aux(sizeLabelW, sizeLabelH, tmp, s);
        }
      }
			else if(this.niv.getPieces()[a][b] instanceof PieceL ) suite_aux2(sizeLabelW, sizeLabelH, tmp, "Images/PieceL", this.niv.getPieces()[a][b]);
      else if(this.niv.getPieces()[a][b] instanceof PieceT ) suite_aux2(sizeLabelW, sizeLabelH, tmp, "Images/PieceT", this.niv.getPieces()[a][b]);
			//piece depart
      else if(this.niv.getPieces()[a][b] instanceof PieceP&&this.niv.getPieces()[a][b].getRotat()==6 ){
        if(a==0) suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart1.png");
        else if(b==0) suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart0.png");
        else if(a==this.niv.getPieces().length-1) suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart2.png");
        else suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceDepart3.png");
      }
			//piece arrivee
			else if(this.niv.getPieces()[a][b]  instanceof Piece&&(this.niv.getPieces()[a][b].getRotat()==1||this.niv.getPieces()[a][b].getRotat()==2||this.niv.getPieces()[a][b].getRotat()==3||this.niv.getPieces()[a][b].getRotat()==4)){
        stocka=a;
        stockb=b;
        arr=tmp;
        if(a==0) suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee3.png");
        else if(b==0) suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee2.png");
        else if(a==this.niv.getPieces().length-1)suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee1.png");
        else suite_aux(sizeLabelW, sizeLabelH, tmp, "Images/PieceArrivee0.png");
      }
			else if(this.niv.getPieces()[a][b]==null ||(!this.niv.getPieces()[a][b].getRotation()[0]&&!this.niv.getPieces()[a][b].getRotation()[1]&&!this.niv.getPieces()[a][b].getRotation()[2]&&!this.niv.getPieces()[a][b].getRotation()[3])){}
   }
	 this.niv.jeu();
   if(this.niv.victoire()) {
      this.setJoueable(false);
      if(stocka==0) suite_aux(sizeLabelW, sizeLabelH, arr, "Images/PieceArriveeFin2.png");
      else if(stockb==0) suite_aux(sizeLabelW, sizeLabelH, arr, "Images/PieceArriveeFin3.png");
      else if(stocka==this.niv.getPieces().length-1) suite_aux(sizeLabelW, sizeLabelH, arr, "Images/PieceArriveeFin1.png");
      else suite_aux(sizeLabelW, sizeLabelH, arr, "Images/PieceArriveeFin.png");
    }
  }

	public void suite_aux(int sizeLabelW, int sizeLabelH, int tmp, String nom) {
		ImageIcon iconLogo = new ImageIcon(new javax.swing.ImageIcon(getClass().getResource(nom)).getImage().getScaledInstance(sizeLabelW, sizeLabelH, Image.SCALE_SMOOTH));
		this.labOff.get(tmp).setIcon(iconLogo);
	}

	public void suite_aux2(int sizeLabelW, int sizeLabelH, int tmp, String nom, Piece p) {
		String s = nom+Integer.toString(p.getRotat());
		if( !p.getEau() ) s = s+".png";
		else s = s+"Eau.png";
		suite_aux(sizeLabelW, sizeLabelH, tmp, s);
	}

  //fonction qui permet de tourner la piece choisie
  public void jouer(){
    for( int i=0;i<this.labOff.size();i++){
          this.indice=i;
          this.labOff.get(i).addMouseListener(new MouseAdapter(){
          final int tmp=Panneau.this.indice;
          public void mouseClicked(MouseEvent e){
                  int a=tmp/Panneau.this.niv.getPieces()[0].length;
                  int b=tmp%Panneau.this.niv.getPieces()[0].length;
                  int h=Panneau.this.niv.getPieces().length;
                  int l=Panneau.this.niv.getPieces()[0].length;

                  if(a==0||b==0||a==h-1||b==l-1){}
                  else{
                    Panneau.this.niv.getPieces()[a][b].tourner();
                    Panneau.this.niv.setModif(Panneau.this.niv.getPieces()[a][b]);
                    Panneau.this.suite(h,l);
                    Panneau.this.niv.setNbreCoups( Panneau.this.niv.getNbreCoups()-1);
                  }
              }
        });
    }
  }
  public void setJoueable(boolean b){
    this.joueable=b;
  }
  public boolean getJoueable(){
    return this.joueable;
  }
  public Niveau getNiveau(){
		return this.niv;
	}
	public void setPiece(Piece p, int x,int y){
		this.niv.getPieces()[x][y]=p;
	}

}
