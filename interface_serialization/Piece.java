import java.io.Serializable;
public class Piece implements Serializable{
  private int x, y;
  private boolean[] rotation;
  private int rotat;
  private boolean eau; //permet de savoir si l'eau passe par cette piece
  private boolean test1; //permet de savoir si cette piece a deja ete testee pour l'emboitement(voir Niveau.java : emboitement_rec)
  private boolean test2; //permet de savoir si cette piece a deja ete testee pour retirer l'eau(voir Niveau.java : retireEau)
  private boolean backtracking; //permet de savoir si cette piece a déjà été testée pour le backtracking(voir Niveau.java : backtracking)

  public Piece(int r){
    this.rotation=new boolean[4];
    for( int i = 0; i < 4; i++) {
      this.rotation[i]=false;
    }
    this.eau=false;
    this.rotat=r;
    this.test1 = false;
    this.test2 = false;
    this.x = 0;
    this.y = 0;
  }

  public void setDepart() {
    for( int i = 0; i < 4; i++) {
        this.rotation[i]=true;
      }
  }
  public int getX(){
    return this.x;
  }
  public void setX(int i) {
    this.x = i;
  }

  public int getY(){
    return this.y;
  }
  public void setY(int i) {
    this.y = i;
  }


  public void tourner(){
    boolean tmp=this.rotation[3];
    this.rotation[3]=this.rotation[2];
    this.rotation[2]=this.rotation[1];
    this.rotation[1]=this.rotation[0];
    this.rotation[0]=tmp;
    if(this.rotat==3) this.rotat=0;
    else this.rotat++;
  }

  public Piece retourner() {
    Piece p = new Piece(0);
    p.rotation[0] = this.rotation[1];
    p.rotation[1] = this.rotation[2];
    p.rotation[2] = this.rotation[3];
    p.rotation[3] = this.rotation[0];
    p.x = this.x;
    p.y = this.y;
    return p;
  }

  public int getRotat() {
    return this.rotat;
  }
  public void setRotat(int r) {
    this.rotat = r;
  }
  public boolean[] getRotation() {
    return this.rotation;
  }
  public void setRotation(boolean[] t) {
    this.rotation=t;
  }
  public void setEau(boolean a) {
    this.eau=a;
  }
  public boolean getEau() {
    return this.eau;
  }
  public void setTest(boolean a) {
    this.test1 = a;
  }
  public boolean getTest() {
    return this.test1;
  }
  public void setTest2(boolean a) {
    this.test2 = a;
  }
  public boolean getTest2() {
    return this.test2;
  }
  public void setBacktracking(boolean a) {
    this.backtracking = a;
  }
  public boolean getBacktracking() {
    return this.backtracking;
  }
}
