import java.io.Serializable;
public class PieceI extends Piece implements Serializable{

  public PieceI(int rotation) {
    super(rotation);
    boolean []tmp={true,false,true,false};
    if(rotation == 1 || rotation == 3) {
      tmp[0] = false;
      tmp[1] = true;
      tmp[2] = false;
      tmp[3] = true;
    }
    super.setRotation(tmp);
  }

}
