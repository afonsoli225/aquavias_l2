import java.io.Serializable;
public class PieceL extends Piece implements Serializable{

  public PieceL(int rotation) {
    super(rotation);
    boolean []tmp={true,true,false,false};
    if(rotation == 1) {
      tmp[0] = false;
      tmp[1] = true;
      tmp[2] = true;
      tmp[3] = false;
    }
    if(rotation == 2) {
      tmp[0] = false;
      tmp[1] = false;
      tmp[2] = true;
      tmp[3] = true;
    }
    if(rotation == 3) {
      tmp[0] = true;
      tmp[1] = false;
      tmp[2] = false;
      tmp[3] = true;
    }
    super.setRotation(tmp);
  }

}
