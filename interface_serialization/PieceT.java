import java.io.Serializable;
public class PieceT extends Piece implements Serializable{

  public PieceT(int rotation) {
    super(rotation);
    boolean []tmp={false,true,true,true};
    if(rotation == 1) {
      tmp[0] = true;
      tmp[1] = false;
      tmp[2] = true;
      tmp[3] = true;
    }
    if(rotation == 2) {
      tmp[0] = true;
      tmp[1] = true;
      tmp[2] = false;
      tmp[3] = true;
    }
    if(rotation == 3) {
      tmp[0] = true;
      tmp[1] = true;
      tmp[2] = true;
      tmp[3] = false;
    }
    super.setRotation(tmp);
  }

}
