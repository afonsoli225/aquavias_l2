import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

public class Serialization {
  private Panneau pan;
  private int max;
  public Serialization(Panneau p){
    this.pan = p;
  }

  public Serialization(int a){
    this.max=a;
  }
  public Serialization(){

  }

  public Panneau getPanneau(){
    return pan;
  }
  public int getMax(){
    return max;
  }

  public void serialize(String str){
    try{
      FileOutputStream fos = new FileOutputStream(str + ".ser");
      ObjectOutputStream oos = new ObjectOutputStream(fos);

      oos.writeObject(this.pan);
      oos.close();
    }catch(FileNotFoundException e){
      e.printStackTrace();
    }catch(IOException e){
      e.printStackTrace();
    }
  }

  public void deserialize(String ser){
    try{
      FileInputStream fis = new FileInputStream(ser);
      ObjectInputStream ois = new ObjectInputStream(fis);
      pan = (Panneau)ois.readObject();
      fis.close(); 
      ois.close(); 
    }catch(FileNotFoundException e){
      e.printStackTrace();
    }catch(IOException e){
      e.printStackTrace();
    }catch(ClassNotFoundException e){
      e.printStackTrace();
    }
  }
  public void save(String str){
    try{
      FileOutputStream fos = new FileOutputStream(str + ".ser");
      ObjectOutputStream oos = new ObjectOutputStream(fos);

      oos.writeObject(this.max);
      oos.close();
    }catch(FileNotFoundException e){
      e.printStackTrace();
    }catch(IOException e){
      e.printStackTrace();
    }
  }

  public void load(String ser){
    try{
      FileInputStream fis = new FileInputStream(ser);
      ObjectInputStream ois = new ObjectInputStream(fis);
      max = (int)ois.readObject();
      fis.close(); 
      ois.close(); 
    }catch(FileNotFoundException e){
      e.printStackTrace();
    }catch(IOException e){
      e.printStackTrace();
    }catch(ClassNotFoundException e){
      e.printStackTrace();
    }
  }
  
}
