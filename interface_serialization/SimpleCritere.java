import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*; 
public class SimpleCritere {
	private JFrame f;
	private JButton b;
	private JSlider slider;
 	private JSlider slider2;
	
	public SimpleCritere(){
		Border blackline= BorderFactory.createLineBorder(Color.black);
		//Nouvelle fenetre
		this.f=new JFrame("Taille du niveau");
		//bouton pour creer
		this.b=new JButton("CREER");
		this.b.setContentAreaFilled(false);
		this.b.setOpaque(true);
		this.b.setBorder(blackline);
		this.b.setBounds(100,100,140, 40);
		//deux sliders pour obtenir la taille du niveau
		this.slider= new JSlider(JSlider.HORIZONTAL, 3,14,5);
		this.slider2 = new JSlider(JSlider.HORIZONTAL, 3,14,5);
		//Personnaliser les sliders
		this.slider.setMinorTickSpacing(1);
    	this.slider.setMajorTickSpacing(1);
    	this.slider.setPaintTicks(true);
    	this.slider.setPaintLabels(true);
    	this.slider.setBorder(BorderFactory.createTitledBorder("Hauteur du niveau"));
    	
    	this.slider2.setMinorTickSpacing(1);
   		this.slider2.setMajorTickSpacing(1);
    	this.slider2.setPaintTicks(true);
    	this.slider2.setPaintLabels(true);
    	this.slider2.setBorder(BorderFactory.createTitledBorder("Largeur du niveau"));
		
		this.f.add(slider);
		this.f.add(slider2);
		this.f.add(b);
		this.f.setSize(500,500);
		this.f.setLayout(new GridLayout(3,1));
		this.f.setVisible(true);
		this.f.setLocationRelativeTo(null);
		this.f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.b.addActionListener( event ->
      		{
        		int hauteur = slider.getValue();
        		int largeur = slider2.getValue();
        		VueCreation cris=new VueCreation(hauteur,largeur);
        		f.dispose();
        	}
    	);
    	this.b.addMouseListener(new java.awt.event.MouseAdapter() {
   			Font n=b.getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		b.setBorderPainted(false);
        	b.setBackground(Color.ORANGE);
        	b.setFont(new Font(n.getName(),n.getStyle(),20));

    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
        	b.setBackground(UIManager.getColor("control"));
        	b.setBorderPainted(true);
        	b.setFont(n);
    		}
    		@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	b.setBorderPainted(false);
        	b.setBackground(Color.GRAY);
        	}
		});
    }
	
	public JSlider getSlider(){
    	return this.slider;
  	}
 }
