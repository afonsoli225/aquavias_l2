import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
public class SimpleJButtonSer {
	private VueCreation vue;
	private JFrame f;
	private JButton b;
	private JButton retour;
	private JLabel label;
	private JLabel label1;
	private JTextField textfield;
	private Serialization ser;

	public SimpleJButtonSer(Serialization ser, VueCreation s){
		this.vue=s;
		this.ser = ser;
		//Nouvelle Fenetre
		this.f=new JFrame("Boutton Sauvegarde");
		//Les boutons de validation et de retour		
		this.b=new JButton("Valider votre niveau");
		this.b.setBounds(250,45,140, 40);
		this.retour=new JButton("Retourner");
		this.retour.setBounds(250,100,140, 40);
		//Les labels nom du niveau et de description
		this.label = new JLabel("Nom du niveau:");
		this.label.setBounds(10, 15, 100, 100);
		this.label1 = new JLabel("Sauvegarder avec un nom qui est un nombre");
		this.label1.setBounds(10, 100, 450, 200);
		//Une zone pour nommer votre niveau		
		this.textfield= new JTextField();
		this.textfield.setBounds(110, 50, 130, 30);
		

		f.add(label1);
		f.add(textfield);
		f.add(label);
		f.add(b);
		f.add(retour);
		f.setSize(460,300);
		f.setLayout(null);
		f.setLocationRelativeTo(null);
		f.setVisible(true);
		f.setDefaultCloseOperation(0);
		f.getRootPane().setDefaultButton(b);

							//action listener
		this.b.addActionListener(new ActionListener() {
			public void go() {
				if(textfield.getText().equals("")||textfield.getText()==null||!isNumeric(textfield.getText())){

				}else{
					ser.serialize(textfield.getText());
					vue.setEnabled(true);
					f.dispose();
				}
				
			}
			@Override
			public void actionPerformed(ActionEvent arg0) {
				this.go();
				label1.setText("<html>Sauvegarder avec un nom qui est un nombre.<br/>Le nom que vous avez choisis ne marche pas, veuillez rentrer un autre nom</html>");
					
				
			}
	      });
		this.retour.addActionListener( event ->
      		{
      			vue.setEnabled(true);
      			vue.getPanneau().getNiveau().getParcours().clear();
      			f.dispose();
      			
      			
        	}
   		);
   		
	}

	public JTextField getTxtFd() {
		return this.textfield;
	}
	public  boolean isNumeric(String strNum) {
    	if (strNum == null) {
        return false;
    	}
    	try {
        	double d = Double.parseDouble(strNum);
   		} catch (NumberFormatException nfe) {
        	return false;
    	}
    	return true;
    }
    public JFrame getFrame(){
    	return this.f;
    }
 }
