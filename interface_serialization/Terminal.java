import java.util.*;
import java.io.*;
import java.util.HashMap;

public class Terminal {
  private Jeu jeu;
  public Terminal() {
    this.jeu = new Jeu();
    this.jouer();
  }

  //Affichage de l'accueil
  public void accueil() {
    System.out.println("\033[34m          〄Bienvenue à Aquavias〄          \033[0m");
    for (int i =0;i<3;i++) {
      System.out.println();
    }
    System.out.println("\033[34m            Jouer (appuyer sur j)          \033[0m");
    System.out.println("\033[34m      Informations sur le jeu (appuyer sur i)          \033[0m");
    System.out.println("\033[34m            Quitter (appuyer sur q)          \033[0m");
  }

  //Print les possibliliés d'hauteur
  public void printAxeX(Niveau niv) {
    int a = niv.getPieces().length-1;
    for (int i=1;i<a;i++) {
      System.out.print("\033[36m"+i+" "+"\033[0m");
    }
  }

  //Print les possibliliés de longueur
  public void printAxeY(Niveau niv) {
    int a = niv.getPieces()[0].length-1;
    for (int i=1;i<a;i++) {
      System.out.print("\033[36m"+i+" "+"\033[0m");
    }
  }

  //Changement de string en bleu
  public String TurnBlue(String s){
    String tmp=s;
    return ("\033[34m"+tmp+"\033[0m");
  }

  //affichage du niveau
  public void affiche(Niveau niv) {
      Piece[][] pieces = niv.getPieces();
        for (int i = 0; i < pieces.length; i++) {
            for (int j = 0; j < pieces[i].length; j++) {
                if (pieces[i][j] instanceof PieceI) {
                  affiche_rec(pieces[i][j], "║", "┃", "═", "━");
                }
                else if (pieces[i][j] instanceof PieceL) {
                  affiche_rec(pieces[i][j], "╚", "┗", "╔", "┏", "╗", "┓", "╝", "┛");
                }
                else if (pieces[i][j] instanceof PieceT) {
                  affiche_rec(pieces[i][j], "╦", "┳", "╣", "┫", "╩", "┻", "╠", "┣");
                }
                else if(pieces[i][j] instanceof PieceP && pieces[i][j].getRotat()!=6){
                  affiche_rec(pieces[i][j], "╬", "╋", "╬", "╋");
                }
                else if( pieces[i][j]==null) {
                  System.out.print("◈");
                }
                else if(pieces[i][j] instanceof Piece&&pieces[i][j].getRotat()==6&&pieces[i][j].getEau()) {
                  System.out.print(TurnBlue("⊙"));
                }
                else if(pieces[i][j] instanceof Piece&&pieces[i][j].getRotat()!=0) {
                  if(pieces[i][j].getEau()) System.out.print(TurnBlue("♨"));
                  else System.out.print("♨");
                }
                else {
                    System.out.print("T" + pieces[i][j].getRotat() + " ");
                }
            }
            System.out.println();
        }
  }
  public void affiche_rec(Piece p, String a1, String a2, String b1, String b2, String c1, String c2, String d1, String d2) {
      if(p.getRotat()==0) {
        if(p.getEau()) {
          System.out.print(TurnBlue(a2));
        }else {
          System.out.print(a1);
        }
      }
      if(p.getRotat()==1) {
        if(p.getEau()) {
          System.out.print(TurnBlue(b2));
        }else {
          System.out.print(b1);
        }
      }

      else if(p.getRotat()==2) {
        if(p.getEau()) {
          System.out.print(TurnBlue(c2));
        }else {
          System.out.print(c1);
        }
      }
      else if(p.getRotat()==3) {
        if(p.getEau()) {
          System.out.print(TurnBlue(d2));
        }else {
          System.out.print(d1);
        }
     }
  }

  public void affiche_rec(Piece p, String a1, String a2, String b1, String b2) {
    affiche_rec(p, a1, a2, b1, b2, a1, a2, b1, b2);
  }

  //readInstruction prend des coordonnées d'une piece et la fait tourner;
  public void readInstruction(int x,int y, Niveau niv) {
    int h = niv.getPieces().length;
    int l = niv.getPieces()[0].length;

    if( x>0 && y>0 && x<h-1 && y<l-1 ) {
      niv.getPieces()[x][y].tourner();
      niv.setModif(niv.getPieces()[x][y]);
    }
    else if( x<=0 || y<=0 || x>=h-1 || y>=l-1 ){
      System.out.println("\033[101mVos choix de coordonnées ne fonctionnent pas dans ce niveau. Veuillez retenter.\033[0m");
    }
  }

  //affiche tous les niveaux disponibles dans le jeu
  public void affichageNiveaux() {
      System.out.print("Choisissez un niveau: [");
      int size = jeu.getNiveaux().size();
      for( int i = 0; i < size; i++ ) {
        if( jeu.getNiveaux().get(i) != null ) {
          if( i+1 != size ) System.out.print(i+",");
          else System.out.println(i+"]");
        }
      }
    }

  //fonction pour jouer sur le terminal
  public void jouer() {
    int count=-1;
    boolean jouer=true;
    this.accueil();
    String s="";
    Scanner cmd = new Scanner(System.in);
    String c =cmd.nextLine();
    while(jouer) {
      if(c.equals("j")||c.equals("J")) {
        this.affichageNiveaux();
        count = cmd.nextInt();
        jouer=false;
      }
      else if(c.equals("r")) {
        this.accueil();
        c =cmd.nextLine();
      }
      else if (c.equals("i")) {
        System.out.println("Aquavias is a puzzle game of increasing difficulty ");
        System.out.println("in which each level is a challenging game of logic and intelligence that will hook you.");
        System.out.println("r pour retourner");
        Scanner tmp=new Scanner(System.in);
        s=tmp.nextLine();
        if(s.equals("r")) c="r";
      }
      else if(c.equals("q")){
        jouer=false;
      }
    }

      if(count!=-1) this.partie(count);
  }
  //une partie de jeu(donc pour un niveau)
  public void partie(int num) {
      int x1,y1;
      Scanner x = new Scanner(System.in);
      boolean jouer = true;

      //on lance le jeu une premiere fois avant de faire jouer le joueur
      jeu.getNiveaux().get(num).emboitement_depart();
      jeu.getNiveaux().get(num).maj_bool_test();
      this.affiche(jeu.getNiveaux().get(num));

      while(jouer) {

        System.out.println("Ordonnée/Ligne ⟹  Possibilités: ");
        this.printAxeX(jeu.getNiveaux().get(num));
        x1 = x.nextInt();

        System.out.println("Abscisse/Colonne ⟹  Possibilités: ");
        this.printAxeY(jeu.getNiveaux().get(num));
        y1= x.nextInt();

        this.readInstruction(x1, y1,jeu.getNiveaux().get(num));
        jeu.getNiveaux().get(num).jeu();
        this.affiche(jeu.getNiveaux().get(num));

        //si la partie est terminée
        if(jeu.getNiveaux().get(num).victoire()) {
          System.out.println("Victoire !!");
          jouer = false;
        }
      }

      //apres avoir fini la partie il faut savoir ce que le joueur compte faire
      int size = jeu.getNiveaux().size();

      //s'il reste des niveaux superieurs auquels le joueur peut jouer
      if( num+1 != size ) {
        while(!jouer) {
          System.out.println("Voulez-vous passer au niveau suivant (appuyer sur s) ou retourner sur l'accueil (appuyer sur a) ?");
          String reponse = x.nextLine();

          if( reponse.equals("a") || reponse.equals("A") ) {
            jouer = true;
            this.jouer();
          }
          else if( reponse.equals("s") || reponse.equals("S") ) {
            jouer = true;
            this.partie(num+1);
          }
        }
      }
      else {
        System.out.println("Nous n'avons pas de niveau supérieur... N'hésitez pas à vous améliorer dans les niveaux précédents !");
        this.jouer();
      }
  }
  public Jeu getJeu(){
    return this.jeu;
  }
}
