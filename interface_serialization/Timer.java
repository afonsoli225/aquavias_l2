import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import javax.swing.border.*; 
public class Timer extends Thread{
	private int count;
	private JButton time;
	private VueJoueur jeu;
	private Music music=new Music();
	private int reperer;
	public Timer(JButton j, VueJoueur v){
		this.time=j;
		this.jeu=v;
		if(v.getPanneau().getNiveau().getOption()[0]&&!v.getPanneau().getNiveau().getOption()[1]){
			this.count=v.getPanneau().getNiveau().getMin()*2+10;
		}else if(v.getPanneau().getNiveau().getOption()[1]&&!v.getPanneau().getNiveau().getOption()[0]){
			this.reperer=v.getPanneau().getNiveau().getNbreCoups();
			this.count=v.getPanneau().getNiveau().getNbreCoups();
		}else if(!v.getPanneau().getNiveau().getOption()[0]&&!v.getPanneau().getNiveau().getOption()[1]){
			this.count=-1;
		}
		
		this.music.playMusic("Images/Eauloop.wav");
	}
	public int returnCount(){
		return count;
	}
	public void run(){
		int i=0;
		if(jeu.getPanneau().getNiveau().getOption()[0]&&!jeu.getPanneau().getNiveau().getOption()[1]){
			while(!jeu.getPanneau().getNiveau().victoire()){
			
				if(count==0){
					count=-1;
					i++;
					if(i<=1&&jeu.getMusic()){
						VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
						jeu.dispose();
						this.music.stopMusic("Images/Eauloop.wav");
					}
				}else if(jeu.getPanneau().getNiveau().getFuites()!=0){
					count--;
					time.setText(Integer.toString(count));
				}if(jeu.getMusic()==false){
					this.music.stopMusic("Images/Eauloop.wav");
				}else{
				}
				try {
					Thread.sleep(1000);
				}catch(InterruptedException e) {
					e.printStackTrace();
				}
			}
			this.music.stopMusic("Images/Eauloop.wav");
			if (jeu.getPanneau().getJoueable()==false){
				try{
					Jeu p=new Jeu();
       				//Quand le joueur gagne la partie
					Music victory=new Music();
       				victory.playMusic("Images/victory.wav");
       				Thread.sleep(2000); 
       				JFrame f=new JFrame("Victoire");
					//niveau suiavnt 
					JButton b=new JButton("Niveau suivant");
					//Recommencer le niveau
					JButton b1=new JButton("Refaire le niveau");
					//Retour au sélection des niveaux
					JButton b2=new JButton();
					b2.setHorizontalAlignment(SwingConstants.CENTER);
					b2.setVerticalAlignment(SwingConstants.CENTER);
					if(jeu.getIndice()+1<p.getNiveaux().size()){
						b2.setText("Choisir un niveau");
					}else{
						b2.setText("<html>Bravo! Vous avez fini tous les niveaux! <br>Vous pouvez maintenant creer des niveaux avec ce pseudo: <br> Aquavias3</html>");
					}
					JLabel victoire=new JLabel("");
					ImageIcon iconLogo = new ImageIcon(new javax.swing.ImageIcon(getClass().getResource("Images/complete.png")).getImage().getScaledInstance(500, 120, Image.SCALE_SMOOTH));
					victoire.setIcon(iconLogo);
					victoire.setBounds(100,100,140, 40);
					f.add(victoire);
					f.add(b);
					f.add(b1);
					f.add(b2);
					f.setSize(500,500);
					f.setLayout(new GridLayout(4,1));
					f.setVisible(true);
					f.setLocationRelativeTo(null);
					f.setDefaultCloseOperation(0);
					jeu.setEnabled(false);
					f.setAlwaysOnTop( true );
					b.addActionListener( event ->
      					{
      				
      					if(jeu.getIndice()+1<p.getNiveaux().size()){
      						VueJoueur n=new VueJoueur(jeu.getIndice()+1,jeu.getUser());
        					n.setMax(jeu.getMax()+1);
        					victory.stopMusic("Images/victory.wav");
        					jeu.dispose();
        					f.dispose();
        				}else{
        					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      						victory.stopMusic("Images/victory.wav");
      						jeu.dispose();
        					f.dispose();
        				}
      					}
    				);
    				b1.addActionListener( event ->
      					{
        				VueJoueur n=new VueJoueur(jeu.getIndice(),jeu.getUser());
        				n.setMax(jeu.getMax());
        				victory.stopMusic("Images/victory.wav");
        				jeu.dispose();
        				f.dispose();
        				}
    				);
    				b2.addActionListener( event ->
      					{
        				if(jeu.getIndice()<jeu.getMax()){
      						VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      						victory.stopMusic("Images/victory.wav");
      					}else{
      						if(jeu.getMax()>=p.getNiveaux().size()-1){
      							VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      							victory.stopMusic("Images/victory.wav");
      						}else{
      							VueNiveau n=new VueNiveau(jeu.getMax()+1,jeu.getUser());
      							victory.stopMusic("Images/victory.wav");
      						}
      					}
      					jeu.dispose();
        				f.dispose(); 
						}
   					);
    				paintButton(b);
    				paintButton(b1);
    				paintButton(b2);
					
				}catch(InterruptedException e){
        			e.printStackTrace();
      			}
			}
		}else if(jeu.getPanneau().getNiveau().getOption()[1]&&!jeu.getPanneau().getNiveau().getOption()[0]){
			
			while(!jeu.getPanneau().getNiveau().victoire()){
			if(count==0){
				count=-1;
				jeu.getPanneau().getNiveau().setNbreCoups(this.reperer);
				i++;
				if(i<=1&&jeu.getMusic()){
					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
					jeu.dispose();
					this.music.stopMusic("Images/Eauloop.wav");
				}else{
					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
					this.music.stopMusic("Images/Eauloop.wav");
				}
				
			}
			
			else if(jeu.getPanneau().getNiveau().getFuites()!=0&& count>0){
				count=jeu.getPanneau().getNiveau().getNbreCoups();

				time.setText(Integer.toString(count));
			}if(jeu.getMusic()==false){
				this.music.stopMusic("Images/Eauloop.wav");
			}
			else{
				
			}
			try {
				Thread.sleep(100);
			}catch(InterruptedException e) {
				
				e.printStackTrace();
			}

			}

			this.music.stopMusic("Images/Eauloop.wav");
			if (jeu.getPanneau().getJoueable()==false){
				try{
				Jeu p=new Jeu();
       			jeu.getPanneau().getNiveau().setNbreCoups(this.reperer);	//Quand le joueur gagne la partie
				Music victory=new Music();
       			victory.playMusic("Images/victory.wav");
       			Thread.sleep(2000); 
       			
				JFrame f=new JFrame("Victoire");
				
				//niveau suiavnt 
				JButton b=new JButton("Niveau suivant");
				//Recommencer le niveau
				JButton b1=new JButton("Refaire le niveau");
				//Retour au sélection des niveaux
				JButton b2=new JButton();
				b2.setHorizontalAlignment(SwingConstants.CENTER);
				b2.setVerticalAlignment(SwingConstants.CENTER);
				if(jeu.getIndice()+1<p.getNiveaux().size()){
					b2.setText("Choisir un niveau");
				}else{
					b2.setText("<html>Bravo! Vous avez fini tous les niveaux! <br>Vous pouvez maintenant creer des niveaux avec ce pseudo: <br> Aquavias3</html>");
				}
				
				JLabel victoire=new JLabel("");
				ImageIcon iconLogo = new ImageIcon(new javax.swing.ImageIcon(getClass().getResource("Images/complete.png")).getImage().getScaledInstance(500, 120, Image.SCALE_SMOOTH));
				victoire.setIcon(iconLogo);
				victoire.setBounds(100,100,140, 40);
				f.add(victoire);
				f.add(b);
				f.add(b1);
				f.add(b2);
				f.setSize(500,500);
				f.setLayout(new GridLayout(4,1));
				f.setVisible(true);
				f.setLocationRelativeTo(null);
				f.setDefaultCloseOperation(0);
				jeu.setEnabled(false);
				f.setAlwaysOnTop( true );
				b.addActionListener( event ->
      				{
      				
      				if(jeu.getIndice()+1<p.getNiveaux().size()){
      					VueJoueur n=new VueJoueur(jeu.getIndice()+1,jeu.getUser());
        			n.setMax(jeu.getMax()+1);
        			victory.stopMusic("Images/victory.wav");
        			jeu.dispose();

        			f.dispose();
        			}else{
        			
      					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      					victory.stopMusic("Images/victory.wav");
      					jeu.dispose();
        				f.dispose();
        			}
					}
    			);
    			b1.addActionListener( event ->
      				{
      				jeu.getPanneau().getNiveau().setNbreCoups(this.reperer);
      				VueJoueur n=new VueJoueur(jeu.getIndice(),jeu.getUser());
        			n.setMax(jeu.getMax());
        			victory.stopMusic("Images/victory.wav");
        			jeu.dispose();
        			f.dispose();
        			}
    			);
    			b2.addActionListener( event ->
      				{
      				jeu.getPanneau().getNiveau().setNbreCoups(this.reperer);
        			if(jeu.getIndice()<jeu.getMax()){
      					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      					victory.stopMusic("Images/victory.wav");
      					
      				}else{
      					if(jeu.getMax()>=p.getNiveaux().size()-1){
      						VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      						victory.stopMusic("Images/victory.wav");
      					}else{
      						VueNiveau n=new VueNiveau(jeu.getMax()+1,jeu.getUser());
      						victory.stopMusic("Images/victory.wav");
      					}
      					
      				}
      				jeu.dispose();
        			f.dispose(); 
					}
   				);
    			paintButton(b);
    			paintButton(b1);
    			paintButton(b2);

      			}catch(InterruptedException e){
        			e.printStackTrace();
      			}
			}
		}else{
			while(!jeu.getPanneau().getNiveau().victoire()){
			
			if(count==0){
				count=-1;
				i++;
				if(i<=1&&jeu.getMusic()){
					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
					jeu.dispose();
					this.music.stopMusic("Images/Eauloop.wav");
				}else{
					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
					this.music.stopMusic("Images/Eauloop.wav");
				}
				
			}
			else if(jeu.getPanneau().getNiveau().getFuites()!=0){
				count=1;
				time.setText("Niveau normal");
			}if(jeu.getMusic()==false){
				this.music.stopMusic("Images/Eauloop.wav");
			}
			else{
				
			}
			try {
				Thread.sleep(1000);
			}catch(InterruptedException e) {
				e.printStackTrace();
			}

			}

			this.music.stopMusic("Images/Eauloop.wav");
			if (jeu.getPanneau().getJoueable()==false){
				try{
       			Jeu p=new Jeu();	//Quand le joueur gagne la partie
				Music victory=new Music();
       			victory.playMusic("Images/victory.wav");
       			Thread.sleep(2000); 
       			
				JFrame f=new JFrame("Victoire");
				
				//niveau suiavnt 
				JButton b=new JButton("Niveau suivant");
				//Recommencer le niveau
				JButton b1=new JButton("Refaire le niveau");
				//Retour au sélection des niveaux
				JButton b2=new JButton();
				b2.setHorizontalAlignment(SwingConstants.CENTER);
				b2.setVerticalAlignment(SwingConstants.CENTER);
				if(jeu.getIndice()+1<p.getNiveaux().size()){
					b2.setText("Choisir un niveau");
				}else{
					b2.setText("<html>Bravo! Vous avez fini tous les niveaux! <br>Vous pouvez maintenant creer des niveaux avec ce pseudo: <br> Aquavias3</html>");
				}
				
				JLabel victoire=new JLabel("");
				ImageIcon iconLogo = new ImageIcon(new javax.swing.ImageIcon(getClass().getResource("Images/complete.png")).getImage().getScaledInstance(500, 120, Image.SCALE_SMOOTH));
				victoire.setIcon(iconLogo);
				victoire.setBounds(100,100,140, 40);
				f.add(victoire);
				f.add(b);
				f.add(b1);
				f.add(b2);
				f.setSize(500,500);
				f.setLayout(new GridLayout(4,1));
				f.setVisible(true);
				f.setLocationRelativeTo(null);
				f.setDefaultCloseOperation(0);
				jeu.setEnabled(false);
				f.setAlwaysOnTop( true );
				b.addActionListener( event ->
      				{
      				
      				if(jeu.getIndice()+1<p.getNiveaux().size()){
      					VueJoueur n=new VueJoueur(jeu.getIndice()+1,jeu.getUser());
        				n.setMax(jeu.getMax()+1);
        				victory.stopMusic("Images/victory.wav");
        				jeu.dispose();
						f.dispose();
        			}else{
        				VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      					victory.stopMusic("Images/victory.wav");
      					jeu.dispose();
        				f.dispose();
        			}
      				}
    			);
    			b1.addActionListener( event ->
      				{
        			VueJoueur n=new VueJoueur(jeu.getIndice(),jeu.getUser());
        			n.setMax(jeu.getMax());
        			victory.stopMusic("Images/victory.wav");
        			jeu.dispose();
        			f.dispose();
        		
					}
    			);
    			b2.addActionListener( event ->
      				{
        			
      				if(jeu.getIndice()<jeu.getMax()){
      					VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      					victory.stopMusic("Images/victory.wav");
      					
      				}else{
      					if(jeu.getMax()>=p.getNiveaux().size()-1){
      						VueNiveau n=new VueNiveau(jeu.getMax(),jeu.getUser());
      						victory.stopMusic("Images/victory.wav");
      					}else{
      						VueNiveau n=new VueNiveau(jeu.getMax()+1,jeu.getUser());
      						victory.stopMusic("Images/victory.wav");
      					}
      				}
      				jeu.dispose();
        			f.dispose(); 
					}
   				);
    			paintButton(b);
    			paintButton(b1);
    			paintButton(b2);

      			}catch(InterruptedException e){
        			e.printStackTrace();
      			}
			}
		}
		
	}
	public void paintButton(JButton b){
		b.setBounds(100,100,140, 40);
		Border blackline= BorderFactory.createLineBorder(Color.gray);
		b.setBorder(blackline);
		b.setContentAreaFilled(false);
		b.setOpaque(true);
		b.addMouseListener(new java.awt.event.MouseAdapter() {
   					Font n=b.getFont();
					@Override
    				public void mouseEntered(java.awt.event.MouseEvent evt) {
    					b.setBorderPainted(false);
        				b.setBackground(Color.ORANGE);
        				b.setFont(new Font(n.getName(),n.getStyle(),20));
					}
    				@Override
    				public void mouseExited(java.awt.event.MouseEvent evt) {
        				b.setBackground(UIManager.getColor("control"));
        				b.setBorderPainted(true);
        				b.setFont(n);
    				}
    				@Override
        			public void mousePressed(MouseEvent e) {
            		
        				b.setBorderPainted(false);
        				b.setBackground(Color.GRAY);
        			}
		});
		
	}
}
