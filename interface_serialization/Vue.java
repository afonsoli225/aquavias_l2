import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import javax.swing.event.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Hashtable;
import javax.swing.border.*; 
import java.util.Random; 

public class Vue{
	private JFrame f;
	private JPanel container = new JPanel();
	private Panneau niveau;
	private Serialization ser;
    private JTextField name=new JTextField ("RENTREZ VOTRE PSEUDO",30);
    private JLabel title=new JLabel("Aquavias");
    private JButton jb2 = new JButton("ENTRER DANS LE JEU");
    private JButton jb3 = new JButton("QUITTER");
    private JButton jb4 = new JButton("Informations");
	
    public Vue(){
        Font font = new Font(Font.DIALOG, Font.ITALIC, 30);
        Font fontTitle = new Font("Baskerville", Font.ITALIC, 130);
		this.f=new JFrame("Aquavias");
		this.container=new JPanelFond(0);
        //Layout pour le jpanel principal
        this.container.setLayout(new BoxLayout(this.container, BoxLayout.Y_AXIS));
        
        int width=(int)f.getToolkit().getScreenSize().getWidth();
        int height=((int)f.getToolkit().getScreenSize().getHeight());
        //Parametre pour la zone de texte
        this.name.selectAll();
        this.name.setMaximumSize(new Dimension(400, name.getPreferredSize().height+25) );
        this.name.setFont(font);
        this.name.setBackground(Color.gray.brighter());
        this.name.setForeground(Color.white);
        this.name.setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        this.name.selectAll();
        //Le titre principal
        this.title.setFont(fontTitle);
        //Decorer les boutons
        this.jb2.setBounds(430, 300, 198,100);
        this.jb2.setFont(font);
        this.jb3.setBounds(430, 300, 198,100);
        this.jb3.setFont(font);
        this.jb4.setBounds(430, 300, 198,100);
        this.jb4.setFont(font);

        this.title.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.name.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.jb2.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.jb3.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.jb4.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.title.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.name.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.jb2.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.jb3.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.jb4.setAlignmentY(Component.CENTER_ALIGNMENT);
        
       
        this.container.add(Box.createRigidArea(new Dimension(0,50)));
        this.container.add(title);
        this.container.add(Box.createRigidArea(new Dimension(0,150)));
        this.container.add(name);
        this.container.add(Box.createRigidArea(new Dimension(0,20)));
        this.container.add(jb2);
        this.container.add(Box.createRigidArea(new Dimension(0,20)));
        this.container.add(jb3);
        this.container.add(Box.createRigidArea(new Dimension(0,20)));
        this.container.add(jb4);

        title.addMouseListener(new MouseAdapter(){
        
            public void mouseClicked(MouseEvent e){
                Vue n=new Vue();
                f.dispose();
            }

        });
       
        title.addMouseMotionListener(new MouseMotionAdapter() {
             @Override
            public void mouseMoved(MouseEvent e) {
                title.setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        
        });
        
        jb2.addMouseListener(new MouseAdapter() {
         Color color = jb2.getForeground();
         public void mouseEntered(MouseEvent me) {
            color = jb2.getForeground();
            jb2.setForeground(Color.gray);
           
         }
         public void mouseExited(MouseEvent me) {
            jb2.setForeground(color);
         }
        });
       
        jb2.addActionListener(new ActionListener() {

            public void go() {
               
                if(name.getText().equals("Maëliss")||name.getText().equals("Nejma")||name.getText().equals("Océane")||name.getText().equals("Afonso")||name.getText().equals("Klimann")||name.getText().equals("Aquavias3")){
                  
                    JFrame frame = new JFrame("Chargement");
                    f.setEnabled(false);
                    frame.setAlwaysOnTop( true );
                    frame.setDefaultCloseOperation(2);
                    final JProgressBar bar = new JProgressBar(0, 100);
                    bar.setStringPainted(true);
                    Thread t = new Thread(){
                    public void run(){
                        Random rand = new Random();
                        for(int i = 0 ; i < 101 ; i=i+2){
                            if(i>=100) frame.dispose();
                            final int percent = i;
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    bar.setValue(percent);
                                }
                            });
                            if(i>=100){
                                Master vue=new Master();
                                f.dispose(); 
                            }  
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {

                            }
                        }
                    }
                    };
                    frame.add(bar);
                    frame.setSize(500, 500);
                    frame.setVisible(true);
                    frame.setLocationRelativeTo(f);
                    t.start();
                
                }else if(isNumeric(name.getText())==true){

                }else{
                    JFrame frame = new JFrame("Chargement");
                    JLabel oldplayer=new JLabel("Rebonjour "+name.getText(), SwingConstants.CENTER);
                    JLabel newplayer=new JLabel("Bienvenue "+name.getText(), SwingConstants.CENTER);
                    f.setEnabled(false);
                    frame.setAlwaysOnTop( true );
                    frame.setDefaultCloseOperation(2);
                    final JProgressBar bar = new JProgressBar(0, 100);
                    bar.setStringPainted(true);
                    Thread t = new Thread(){
                    public void run(){
                        for(int i = 0 ; i < 101 ; i=i+2){
                            if(i==100) frame.dispose();
                            final int percent = i;
                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    if(percent<=30){
                                        bar.setValue(percent);
                                        bar.setString("Jeu entrain de charger");
                                    }else if(percent > 30 && percent < 70){
                                        bar.setValue(percent);
                                        bar.setString("Veuiller patienter");
                                    }else{
                                        bar.setValue(percent);
                                        bar.setString("Le jeu va bientot commencer");
                                    }
                                   
                                }
                            });
                            if(i==100){
                                File tmpDir = new File(name.getText()+".ser");
                                boolean exists = tmpDir.exists();
                                if(exists==false){
                                    VueNiveau vue= new VueNiveau(name.getText());
                                    f.dispose();
                                }else{
                                    Serialization ser1 = new Serialization();
                                    ser1.load(name.getText()+".ser");
                                    int max = ser1.getMax();
                                    VueNiveau vue= new VueNiveau(max,name.getText());
                                    f.dispose();
                            }
                            }  
                            try {
                                Thread.sleep(100);
                            } catch (InterruptedException e) {

                            }
                        }
                    }
                    };
                    File tmpDir = new File(name.getText()+".ser");
                    boolean exists = tmpDir.exists();
                    if(exists==true) {
                        oldplayer.setBounds(100,100,140, 40);
                        JPanel p=new JPanel();
                        p.add(oldplayer);
                        

                        frame.setContentPane(p);
                    }else{
                        newplayer.setBounds(100,100,140, 40);
                        JPanel p=new JPanel();
                        p.add(newplayer);
                        frame.setContentPane(p);
                    }
                    frame.setLayout(new GridLayout(0,1));
                    frame.add(bar);
                    frame.setSize(500,500);
                    frame.setVisible(true);
                    frame.setLocationRelativeTo(f);
                    t.start();

                    
                }
                
            }
            @Override
            public void actionPerformed(ActionEvent arg0) {
                this.go();
            }
          });
        jb3.addMouseListener(new MouseAdapter() {
         Color color = jb3.getForeground();
         public void mouseEntered(MouseEvent me) {
            color = jb3.getForeground();
            jb3.setForeground(Color.gray);
         }
         public void mouseExited(MouseEvent me) {
            jb3.setForeground(color);
         }
        });
        jb3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent arg0) {
              System.exit(0);
            }
        }); 
        jb4.addMouseListener(new MouseAdapter() {
         Color color = jb4.getForeground();
         public void mouseEntered(MouseEvent me) {
            color = jb4.getForeground();
            jb4.setForeground(Color.gray); 
         }
         public void mouseExited(MouseEvent me) {
            jb4.setForeground(color);
         }
        });
        jb4.addActionListener( event ->
            {   
                JScrollPaneDemo info=new JScrollPaneDemo(this);
                info.start();
            }
        ); 
        name.addFocusListener(new FocusListener() {
            @Override public void focusLost(final FocusEvent pE) {}
            @Override public void focusGained(final FocusEvent pE) {
                name.selectAll();
            }
        });
        f.getRootPane().setDefaultButton(jb2);
        name.requestFocus();
        f.setContentPane(this.container);
		f.setSize((int)f.getToolkit().getScreenSize().getWidth(), ((int)f.getToolkit().getScreenSize().getHeight() - 40));
    	f.setVisible(true);
    	f.setLocationRelativeTo(null);
    	f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
    public JFrame getFrame(){
        return this.f;
    }
    public JTextField getTextField(){
        return this.name;
    }
    
    public  boolean isNumeric(String strNum) {
    if (strNum == null) {
        return false;
    }
    try {
        double d = Double.parseDouble(strNum);
    } catch (NumberFormatException nfe) {
        return false;
    }
    return true;
}
	
	
}
