import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Hashtable;
import javax.swing.border.*; 
import java.io.*;
public class VueCreation extends JFrame {
	//tous les boutons à placer dans le panneau à droite
	private JButton pieceI0 = new JButton();
	private JButton pieceI1 = new JButton();
	private JButton pieceT0 = new JButton();
	private JButton pieceT1 = new JButton();
	private JButton pieceT2 = new JButton();
	private JButton pieceT3 = new JButton();
	private JButton pieceP = new JButton();
	private JButton pieceL0 = new JButton();
	private JButton pieceL1 = new JButton();
	private JButton pieceL2 = new JButton();
	private JButton pieceL3 = new JButton();
	private JButton pieceA = new JButton();
	private JButton pieceD = new JButton();
	private JButton creer = new JButton("Sauvegarder votre niveau");
	private JButton retour= new JButton("retour ");
	private JRadioButton fuites=new JRadioButton("Eau qui fuit");
	private JRadioButton nbreCoup=new JRadioButton("Nombre de coup");
	private JButton option= new JButton("valider option ");
  private JButton clean= new JButton("enlever les options");
	private ButtonGroup groupe= new ButtonGroup(); 

 	//Un Jpanel niveau avec hauteur et largeur
	private int height,length;
	private Panneau niveau;
	
	private Thread t = null;
	private Serialization ser;
	private JPanel container=new JPanel();

	public VueCreation(int x,int y){
		this.height=x;
		this.length=y;
    //initialisation avec dimension
		start(height,length);
	}
	
  public void start(int x,int y){
    Border blackline= BorderFactory.createRaisedBevelBorder();
		GridLayout experimentLayout = new GridLayout(0,3);
    this.container.setLayout(new BorderLayout());
		
    JPanel panright=new JPanel();
		panright.setBackground(Color.white);
    panright.setLayout(experimentLayout);
		this.container.add(panright, BorderLayout.EAST);
    this.niveau=new Panneau(x,y);
    this.container.add(this.niveau);
		
		//this.niveau.add(this.creer);
	 //Placement des boutons/Pieces à droite
		placeButton(panright);
    panright.add(this.creer);
		panright.add(this.retour);
		
		//action sur tous les boutons
		pieceI0.addActionListener( event ->
      { 

        this.niveau.setSelected(new PieceI(0));
      }
   	);
   	pieceI1.addActionListener( event ->
      {
        this.niveau.setSelected(new PieceI(1));
      }
   	);
   	pieceT0.addActionListener( event ->
      {
        this.niveau.setSelected(new PieceT(0));
      }
   	);
   	pieceT1.addActionListener( event ->
      {
        this.niveau.setSelected(new PieceT(1));
      }
   	);
   	pieceT2.addActionListener( event ->
      {
        this.niveau.setSelected(new PieceT(2));
      }
   	);
   	pieceT3.addActionListener( event ->
      {
      	this.niveau.setSelected(new PieceT(3));
      }
   	);
   	pieceP.addActionListener( event ->
      {
      	this.niveau.setSelected(new PieceP(1));
      }
   	);
   	pieceL0.addActionListener( event ->
      {
      	this.niveau.setSelected(new PieceL(0));
      }
   	);
   	pieceL1.addActionListener( event ->
      {
      	this.niveau.setSelected(new PieceL(1));
      }
   	);
   	pieceL2.addActionListener( event ->
      {
      	this.niveau.setSelected(new PieceL(2));
      }
   	);
   	pieceL3.addActionListener( event ->
      {
        this.niveau.setSelected(new PieceL(3));
      }
   	);
   	pieceD.addActionListener( event ->
      {	
      	PieceP p=new PieceP(6) ;
      	p.setEau(true);
      	this.niveau.setSelected(p);
      }
    );
   	pieceA.addActionListener( event ->
      {
      	this.niveau.setSelected(new Piece(1));
      }
   	);
		creer.addActionListener( event ->
      { 
        this.niveau.getNiveau().trouverSolutions();
        if(this.niveau.getNiveau().getParcours().size()!=0){
          this.niveau.getNiveau().setNbreCoups(this.niveau.getNiveau().getMin()+2);
          ser = new Serialization(this.niveau);
        	SimpleJButtonSer txt = new SimpleJButtonSer(ser,this);
          this.setEnabled(false);
          txt.getFrame().setAlwaysOnTop( true );
       	}else{
                              
            this.creer.setText("Ce niveau est infaisable");
            ;
        }
      }
    );
		retour.addActionListener( event ->
      {
      	Master mas=new Master();
      	this.dispose();
      }
   	);
    GridLayout little = new GridLayout(2,0);
    JPanel little_option=new JPanel();
    little_option.setLayout(little);
   	panright.add(little_option);
    little_option.add(this.fuites);
   	little_option.add(this.nbreCoup);
   	groupe.add(this.fuites); 
    groupe.add(this.nbreCoup);
    panright.add(this.option);
    panright.add(this.clean);
   	if (fuites.isSelected()){
   			//
   	}if(nbreCoup.isSelected()){
   			//
   	}
   	option.addActionListener(new ActionListener() { 
            // Anonymous class. 
        public void actionPerformed(ActionEvent e) 
            { 
                
            
                if (VueCreation.this.fuites.isSelected()) { 
  					       VueCreation.this.niveau.getNiveau().setOption(0,true);
                   VueCreation.this.niveau.getNiveau().setOption(1,false);
                   VueCreation.this.option.setText("<html>Option fuites <br /><br />appuyer pour changer</html>");
                  
                }else if (VueCreation.this.nbreCoup.isSelected()) { 
  					       VueCreation.this.niveau.getNiveau().setOption(1,true);
                   VueCreation.this.niveau.getNiveau().setOption(0,false);
                   VueCreation.this.option.setText("<html>Option nombres de coup <br /><br />appuyer pour changer</html>");
                }
            
                  
            } 
    });
    clean.addActionListener(new ActionListener() { 
            // Anonymous class. 
        public void actionPerformed(ActionEvent e) 
            { 
               groupe.clearSelection();
                VueCreation.this.niveau.getNiveau().setOption(3,false);
               option.setText("valider option ");
            } 
    });

    setEsthetiqueJButton(this.creer);
    setEsthetiqueJButton(this.retour);
    setEsthetiqueJButton(this.option);
    setEsthetiqueJButton(this.clean);
    panright.setPreferredSize(new Dimension(603, 0));
    this.setContentPane(container);
    this.setSize((int)getToolkit().getScreenSize().getWidth(), ((int)getToolkit().getScreenSize().getHeight() - 40));
    this.setVisible(true);
    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  }
	public Panneau getPanneau(){
		return this.niveau;
	}
	//placement des bouttons dans panright avec leurs images
	public void placeButton(JPanel panright){
    //les images
		ImageIcon i0 = new ImageIcon("Images/PieceI0.png");
		ImageIcon i1 = new ImageIcon("Images/PieceI1.png");
		ImageIcon l0 = new ImageIcon("Images/PieceL0.png");
		ImageIcon l1 = new ImageIcon("Images/PieceL1.png");
		ImageIcon l2 = new ImageIcon("Images/PieceL2.png");
		ImageIcon l3 = new ImageIcon("Images/PieceL3.png");
		ImageIcon p  = new ImageIcon("Images/PieceP.png");
		ImageIcon t0 = new ImageIcon("Images/PieceT0.png");
		ImageIcon t1 = new ImageIcon("Images/PieceT1.png");
		ImageIcon t2 = new ImageIcon("Images/PieceT2.png");
		ImageIcon t3 = new ImageIcon("Images/PieceT3.png");
		ImageIcon d  = new ImageIcon("Images/PieceDepart0.png");
		ImageIcon a  = new ImageIcon("Images/PieceArrivee2.png");
		//mettre les images dans les boutons
    pieceI0.setIcon(i0);
		pieceI1.setIcon(i1);
		pieceL0.setIcon(l0);
		pieceL1.setIcon(l1);
		pieceL2.setIcon(l2);
		pieceL3.setIcon(l3);
		pieceP.setIcon(p);
		pieceT0.setIcon(t0);
		pieceT1.setIcon(t1);
		pieceT2.setIcon(t2);
		pieceT3.setIcon(t3);
		pieceD.setIcon(d);
		pieceA.setIcon(a);
    //Rajouter une bordure
    Border blackline= BorderFactory.createRaisedBevelBorder();
    pieceI0.setBorder(blackline);
    pieceI1.setBorder(blackline);
    pieceL0.setBorder(blackline);
    pieceL1.setBorder(blackline);
    pieceL2.setBorder(blackline);
    pieceL3.setBorder(blackline);
    pieceP.setBorder(blackline);
    pieceT0.setBorder(blackline);
    pieceT1.setBorder(blackline);
    pieceT2.setBorder(blackline);
    pieceT3.setBorder(blackline);
    pieceD.setBorder(blackline);
    pieceA.setBorder(blackline);
		//rajouter dans le jpanel a droite
    panright.add(pieceI0);
		panright.add(pieceI1);
		panright.add(pieceL0);
		panright.add(pieceL1);
		panright.add(pieceL2);
		panright.add(pieceL3);
		panright.add(pieceP);
		panright.add(pieceT0);
		panright.add(pieceT1);
		panright.add(pieceT2);
		panright.add(pieceT3);
		panright.add(pieceD);
		panright.add(pieceA);
	}
  public void setEsthetiqueJButton(JButton b){
    Border blackline= BorderFactory.createLineBorder(Color.black);
    b.setContentAreaFilled(false);
    b.setBackground(Color.WHITE);
    b.setOpaque(true);
    b.setPreferredSize(new Dimension(100, 100));
    b.setBorder(blackline);

      
    b.addMouseListener(new java.awt.event.MouseAdapter() {
      Font n=b.getFont();
      @Override
        public void mouseEntered(java.awt.event.MouseEvent evt) {
        b.setBorderPainted(false);
          b.setBackground(Color.ORANGE);
          b.setFont(new Font(n.getName(),n.getStyle(),15));
        }
        @Override
        public void mouseExited(java.awt.event.MouseEvent evt) {
        b.setBackground(UIManager.getColor("control"));
        b.setBorderPainted(true);
        b.setFont(n);
          }
          @Override
          public void mousePressed(MouseEvent e) {
            //turn red
          b.setBorderPainted(false);
          b.setBackground(Color.GRAY);
          }
    });
  }
}