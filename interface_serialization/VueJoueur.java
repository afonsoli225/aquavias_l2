import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Hashtable;
import javax.swing.border.*; 
import java.util.Random; 


@SuppressWarnings("serial")
public class VueJoueur extends JFrame {
	private Panneau pan;
	private Thread t = null;
	private Serialization ser=new Serialization();
	private JPanel container = new JPanel();
	private JButton creer ;
	private JButton retour= new JButton("Retour au choix");
	private JButton option= new JButton("");
	private Timer timer;
	private int indice;
	public int indiceMax;
	private String user;
	private boolean music=true;
	private int max;
	public VueJoueur(int i,String s){
		this.indice=i;
		this.user=s;
		start(indice);
	}
	
	public void start(int indice){
		this.creer=new JButton("Vous êtes au niveau "+Integer.toString(indice));
		this.container.setOpaque(false);
		this.container.setLayout(new BoxLayout(this.container, BoxLayout.Y_AXIS));
		JPanel panright=new JPanel();
		panright.setBackground(Color.white);
		Jeu jeu =new Jeu();
		String h=jeu.getLevel().get(jeu.getNiveaux().get(indice))+".ser";
		
		this.ser.deserialize(h);
		this.pan=new Panneau(this.ser.getPanneau().getNiveau());
		this.max=this.getPanneau().getNiveau().getNbreCoups();
		int x=this.pan.getNiveau().getPieces().length;
    	int y=this.pan.getNiveau().getPieces()[0].length;
    	int taille=x-y;
    	int tailleP=(25*taille*taille)/11+(625*taille)/11+400;
    	panright.setPreferredSize(new Dimension(0, 50));
    	panright.setMaximumSize(new Dimension(800,50) );
		if(x==y){
			this.pan.setMaximumSize(new Dimension(700,700) );
		}else if(x<y){
			this.pan.setMaximumSize(new Dimension(700+(y-x)*65,700) );
		}else{
			this.pan.setMaximumSize(new Dimension(700-(x-y)*48,700) );
		}
		this.container.add(panright, BorderLayout.NORTH);
		GridLayout experimentLayout = new GridLayout(0,3);
		panright.setLayout(experimentLayout);
		panright.add(this.creer);
		panright.add(this.retour);
		panright.add(this.option);
		this.container.add(this.pan);
		Border loweredetched=BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
		this.retour.setBorder(loweredetched);
		timer = new Timer(option,this);
		timer.start();
		if(this.pan.getJoueable()==false){
			this.dispose();
		}
		retour.addActionListener( event ->
      		{
        		if(this.getIndice()<this.getMax()){
      				VueNiveau n=new VueNiveau(this.getMax(),this.user);
      				this.getPanneau().getNiveau().setNbreCoups(this.max);
      				this.setMax(this.getMax());
      				this.setMusic(false);
      				this.dispose();
      					
      			}else{
      				VueNiveau n=new VueNiveau(this.getMax(),this.user);
      				this.setMax(this.getMax());
      				this.getPanneau().getNiveau().setNbreCoups(this.max);
      				this.setMusic(false);
      				this.dispose();
      			}
      			}
    	);
    	paintButton(this.creer);
		paintButton(this.retour);
		paintButton(this.option);
		this.setContentPane(container);
    	this.setSize((int)getToolkit().getScreenSize().getWidth(), ((int)getToolkit().getScreenSize().getHeight() - 40));
    	this.setVisible(true);
    	this.setDefaultCloseOperation(0);


	}
	public Panneau getPanneau(){
		return this.pan;
	}
	public void setMusic(boolean b){
		this.music=b;
	}
	public boolean getMusic(){
		return this.music;
	}

	public String getUser(){
		return this.user;
	}
	public int getIndice(){
		return this.indice;
	}
	public void setMax(int c){
		this.indiceMax=c;
	}
	public int getMax(){
		return this.indiceMax;
	}
	public void setMaxCoup(int a){
		this.max=a;
	}
	public void paintButton(JButton b){
		Border blackline= BorderFactory.createLineBorder(Color.gray);
		b.setBorder(blackline);
		b.setContentAreaFilled(false);
		b.setOpaque(true);
		b.addMouseListener(new java.awt.event.MouseAdapter() {
   					Font n=b.getFont();
					@Override
    				public void mouseEntered(java.awt.event.MouseEvent evt) {
    					b.setBorderPainted(false);
        				b.setBackground(Color.ORANGE);
        				b.setFont(new Font(n.getName(),n.getStyle(),20));
					}
    				@Override
    				public void mouseExited(java.awt.event.MouseEvent evt) {
        				b.setBackground(UIManager.getColor("control"));
        				b.setBorderPainted(true);
        				b.setFont(n);
    				}
    				@Override
        			public void mousePressed(MouseEvent e) {
            		
        				b.setBorderPainted(false);
        				b.setBackground(Color.GRAY);
        			}
		});
		
	}


}