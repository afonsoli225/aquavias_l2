import java.awt.*;
import java.util.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Hashtable;
import java.io.*;
import javax.swing.border.*; 

@SuppressWarnings("serial")
public class VueNiveau extends JFrame {
	private ArrayList<JButton> labOff=new ArrayList<JButton>();
	private JButton retour=new JButton("retour");
	private Jeu jeu=new Jeu();
	private JPanel container = new JPanelFond(2);
	private boolean[] nivJouable;
	private String user;
	private Music music=new Music();


	public VueNiveau(String s){
		this.nivJouable=new boolean[this.jeu.getNiveaux().size()];
		this.nivJouable[0]=true;
		this.user=s;
		start();
	}
	public VueNiveau(int c,String s){
		this.nivJouable=new boolean[this.jeu.getNiveaux().size()];
		for(int i=0;i<c+1;i++){
			this.nivJouable[i]=true;
		}
		this.user=s;
		start2(c);
	}
	public void start(){
		int a=this.jeu.getNiveaux().size();
		int count =0;
		Border blackline= BorderFactory.createLineBorder(Color.black);
		for (int i=0;i<a;i++){
			if(this.jeu.getNiveaux().get(i)!=null){
				JButton button = new JButton(String.valueOf(i));
        		button.setBackground(Color.red);
        		button.setContentAreaFilled(false);
        		button.setOpaque(true);
				button.setPreferredSize(new Dimension(100, 100));
				button.setBorder(blackline);
				labOff.add(button);
				count++;
			}
		}
		for (int t = 0; t < labOff.size(); t++) {
			final int i=t;//
			labOff.get(i).setBackground(Color.red);
			labOff.get(i).addMouseListener(new java.awt.event.MouseAdapter() {
   			Font n=labOff.get(i).getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		labOff.get(i).setOpaque(true);
    		labOff.get(i).setBorderPainted(false);
        	labOff.get(i).setBackground(Color.ORANGE);
        	labOff.get(i).setFont(new Font(n.getName(),n.getStyle(),20));

    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
        	labOff.get(i).setBackground(UIManager.getColor("control"));
        	labOff.get(i).setBorderPainted(true);
        	labOff.get(i).setFont(n);
    		}
    		@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	labOff.get(i).setBorderPainted(false);
        	labOff.get(i).setBackground(Color.GRAY);
        	}
		});

		}
		JPanel pan=new JPanel();
		this.container.setLayout(new GridBagLayout());
		//this.container.setLayout(new GridBagLayout());
		//pan.setPreferredSize(new Dimension(750, 150));
		int taille=2;
		while((count+1)%taille!=0){
			taille++;
		}
		GridLayout experimentLayout = new GridLayout(0,taille);
		
		pan.setLayout(experimentLayout);
		this.container.add(pan);
		for (int i = 0; i < labOff.size(); i++) {
			final int tmp=i;
			if(!labOff.get(i).getText().equals("")){

      			pan.add(labOff.get(i));
      			labOff.get(i).setOpaque(false);
      			if(this.nivJouable[i]){
      				labOff.get(i).addActionListener( event ->
      				{	
      					String s=VueNiveau.this.labOff.get(tmp).getText();
      					String h=VueNiveau.this.jeu.getLevel().get(VueNiveau.this.jeu.getNiveaux().get(tmp));
      					VueJoueur n=new VueJoueur(tmp,VueNiveau.this.user);
      					n.setMax(tmp);
      					VueNiveau.this.dispose();
        			}
   				);

      			}
      			
      		}
    	}
    	retour.setContentAreaFilled(false);
       	retour.setBackground(Color.red);
		retour.setOpaque(true);
		retour.setPreferredSize(new Dimension(100, 100));
		retour.setBorder(blackline);
		setEsthetiqueJButton(this.retour);


    	pan.add(this.retour);
    	this.retour.addActionListener( event ->
      		{	
      			Vue n=new Vue();
      			this.dispose();
      		}
   		); 
		this.setContentPane(container);
    	this.setSize((int)getToolkit().getScreenSize().getWidth(), ((int)getToolkit().getScreenSize().getHeight() - 40));
    	this.setVisible(true);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void start2(int c){
		int a=this.jeu.getNiveaux().size();
		int count =0;
		Border blackline= BorderFactory.createLineBorder(Color.black);
		for (int i=0;i<a;i++){
			if(this.jeu.getNiveaux().get(i)!=null){
				JButton button = new JButton(String.valueOf(i));
        		button.setBackground(Color.red);
        		button.setContentAreaFilled(false);
        		button.setOpaque(true);
				button.setPreferredSize(new Dimension(100, 100));
				button.setBorder(blackline);
				labOff.add(button);
				count++;
			}
		}
		for (int t = 0; t < labOff.size(); t++) {
			final int i=t;
			labOff.get(i).setBackground(Color.red);
			labOff.get(i).addMouseListener(new java.awt.event.MouseAdapter() {
   			Font n=labOff.get(i).getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		labOff.get(i).setOpaque(true);
    		labOff.get(i).setBorderPainted(false);
        	labOff.get(i).setBackground(Color.ORANGE);
        	labOff.get(i).setFont(new Font(n.getName(),n.getStyle(),20));

    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
        	labOff.get(i).setBackground(UIManager.getColor("control"));
        	labOff.get(i).setBorderPainted(true);
        	labOff.get(i).setFont(n);
    		}
    		@Override
        	public void mousePressed(MouseEvent e) {
            
        	labOff.get(i).setBorderPainted(false);
        	labOff.get(i).setBackground(Color.GRAY);
        	}
		});

		}
		JPanel pan=new JPanel();
		
		this.container.setLayout(new GridBagLayout());
		
		int taille=2;
		while((count+1)%taille!=0){
			taille++;
		}
		GridLayout experimentLayout = new GridLayout(0,taille);
		
		pan.setLayout(experimentLayout);
		this.container.add(pan);
		for (int i = 0; i < labOff.size(); i++) {
			final int tmp=i;
			final int tmp2=c;
			if(!labOff.get(i).getText().equals("")){

      			pan.add(labOff.get(i));
      			labOff.get(i).setOpaque(false);
      			if(this.nivJouable[i]){
      				labOff.get(i).addActionListener( event ->
      				{	
      					String s=VueNiveau.this.labOff.get(tmp).getText();
      					String h=VueNiveau.this.jeu.getLevel().get(VueNiveau.this.jeu.getNiveaux().get(tmp));
      					System.out.println(tmp);
      					System.out.println("allo :"+h);
      					VueJoueur n=new VueJoueur(tmp,VueNiveau.this.user);
      					n.setMax(tmp2);
      					VueNiveau.this.dispose();
        			}
   				);
      			}
      			
      		}
    	}
    	retour.setContentAreaFilled(false);
       	retour.setBackground(Color.red);
		retour.setOpaque(true);
		retour.setPreferredSize(new Dimension(100, 100));
		retour.setBorder(blackline);
		setEsthetiqueJButton(this.retour);
    	pan.add(this.retour);

    	this.retour.addActionListener( event ->
      		{	
      			Serialization ser=new Serialization(c);
      	
      			try {         
					File f= new File(VueNiveau.this.user+".ser");         
					if(f.delete()){  
						
						System.out.println("Joueur sauvegardé");
      					ser.save(VueNiveau.this.user); 
					}else { 
						ser.save(VueNiveau.this.user);
						System.out.println("failed");  
					}  
				} catch(Exception e){  
					e.printStackTrace();  
				}  
      			Vue n=new Vue();
      			this.dispose();
        	}
   		); 
   		
		this.setContentPane(container);
    	this.setSize((int)getToolkit().getScreenSize().getWidth(), ((int)getToolkit().getScreenSize().getHeight() - 40));
    	this.setVisible(true);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public String getUser(){
		return this.user;
	}
	public void setEsthetiqueJButton(JButton b){
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			Font n=b.getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		b.setBorderPainted(false);
        	b.setBackground(Color.ORANGE);
        	b.setFont(new Font(n.getName(),n.getStyle(),20));
    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
    		b.setBackground(UIManager.getColor("control"));
    		b.setBorderPainted(true);
    		b.setFont(n);
        	}
        	@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	b.setBorderPainted(false);
        	b.setBackground(Color.GRAY);
        	}
		});
	}

}