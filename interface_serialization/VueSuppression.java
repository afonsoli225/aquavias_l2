import java.util.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.io.*;
import javax.swing.border.*; 

@SuppressWarnings("serial")
public class VueSuppression extends JFrame{
	private ArrayList<JButton> labOff=new ArrayList<JButton>();
	private JButton retour=new JButton("retour");
	private Jeu jeu=new Jeu();
	private JPanel container = new JPanelFond(2);


	public VueSuppression(){
		//initiliaser la page pour supprimer les niveau
		start();
	}
	public void start(){
		int a=this.jeu.getNiveaux().size();
		int count =0;
		Border blackline= BorderFactory.createLineBorder(Color.black);
		//rajouter les boutons dans la liste laboff
		for (int i=0;i<a;i++){
			if(this.jeu.getNiveaux().get(i)!=null){
				JButton button = new JButton(String.valueOf(i));
        		button.setBackground(Color.red);
        		button.setContentAreaFilled(false);
        		button.setOpaque(true);
				button.setPreferredSize(new Dimension(100, 100));
				button.setBorder(blackline);
				labOff.add(button);
				count++;
			}
		}
		//esthetique de chaque boutons
		for (int t = 0; t < labOff.size(); t++) {
			final int i=t;//
			labOff.get(i).setBackground(Color.red);
			labOff.get(i).addMouseListener(new java.awt.event.MouseAdapter() {
   			Font n=labOff.get(i).getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		labOff.get(i).setOpaque(true);
    		labOff.get(i).setBorderPainted(false);
        	labOff.get(i).setBackground(Color.ORANGE);
        	labOff.get(i).setFont(new Font(n.getName(),n.getStyle(),20));

    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
        	labOff.get(i).setBackground(UIManager.getColor("control"));
        	labOff.get(i).setBorderPainted(true);
        	labOff.get(i).setFont(n);
    		}
    		@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	labOff.get(i).setBorderPainted(false);
        	labOff.get(i).setBackground(Color.GRAY);
        	}
			});
		}
		
		this.container.setLayout(new GridBagLayout());
		int taille=2;
		while((count+1)%taille!=0){
			taille++;
		}
		
		JPanel pan=new JPanel();
		GridLayout experimentLayout = new GridLayout(0,taille);
		pan.setLayout(experimentLayout);
		

		for (int i = 0; i < labOff.size(); i++) {
			final int tmp=i;
			pan.add(labOff.get(i));
      		labOff.get(i).setOpaque(false);
      		labOff.get(i).addActionListener( event ->
      			{	
      				String stmp=VueSuppression.this.jeu.getLevel().get(VueSuppression.this.jeu.getNiveaux().get(tmp));
      				try {         
						File f= new File(stmp+".ser");         
						if(f.delete()){  
							
      					}else { 
						}  
					}catch(Exception e){  
						e.printStackTrace();  
					}
					Master n=new Master();
					VueSuppression.this.dispose(); 
        		}
   			);
      	}
    	//esthetique du bouton de retour
    	retour.setContentAreaFilled(false);
       	retour.setBackground(Color.WHITE);
		retour.setOpaque(true);
		retour.setPreferredSize(new Dimension(100, 100));
		retour.setBorder(blackline);
		setEsthetiqueJButton(this.retour);
    	
    	pan.add(this.retour);
    	this.container.add(pan);
    	
    	//fonctionnalite du bouton-retour à Master
    	this.retour.addActionListener( event ->
      		{	
      			Master n=new Master();
      			this.dispose();
      		}
   		); 
		this.setContentPane(container);
    	this.setSize((int)getToolkit().getScreenSize().getWidth(), ((int)getToolkit().getScreenSize().getHeight() - 40));
    	this.setVisible(true);
    	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void setEsthetiqueJButton(JButton b){
		b.addMouseListener(new java.awt.event.MouseAdapter() {
			Font n=b.getFont();
			@Override
    		public void mouseEntered(java.awt.event.MouseEvent evt) {
    		b.setBorderPainted(false);
        	b.setBackground(Color.ORANGE);
        	b.setFont(new Font(n.getName(),n.getStyle(),20));
    		}
    		@Override
    		public void mouseExited(java.awt.event.MouseEvent evt) {
    		b.setBackground(UIManager.getColor("control"));
    		b.setBorderPainted(true);
    		b.setFont(n);
        	}
        	@Override
        	public void mousePressed(MouseEvent e) {
            //turn red
        	b.setBorderPainted(false);
        	b.setBackground(Color.GRAY);
        	}
		});
	}
}